<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\constant;

class permissions
{
	const U_NEWS_REQUESTER = "u_changecover_tabnews_requester";
	const A_NEWS_MANAGER = "a_changecover_tabnews_approver";
	const U_COVER_REQUESTER = "u_changecover_requester";
	const U_COVER_APPROVER = "u_changecover_approver";
	const U_CAN_VIEW_HIDE_INDEX = "u_changecover_view_hide_index";
	const A_EDIT_INDEX = "a_changecover_edit_index";
	const A_API = "a_changecover_api";
}
