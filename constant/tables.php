<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\constant;

class tables
{
	public const NEWS = "changecover_news";
	public const COVER_TOAPPROVE = "changecover_toapprove";
	public const COMICS_INDEX = "comics_index";
	public const MONTHLY_RELEASE = "monthly_release";

	public const USERS = "users"; // todo: replace by phpbb built-in constant
	public const CONFIG_TEXT = "config_text";

	public const COMICS_INDEX_COLUMNS = [
		'id',
		'title',
		'sorted_title',
		'sort',
		'url',
		'hide',
		'status',
		'editor',
	];
}
