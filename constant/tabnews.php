<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\constant;

class tabnews
{
	const TABNEWS_1 = "tabnews_urlpage";   // "News"
	const TABNEWS_2 = "tabnews2_urlpage";  // "DC Rebirth"
	const TABNEWS_3 = "tabnews_text";      // "DC Hors Rebirth"
	const TABNEWS_4 = "tabnews4_urlpage";  // "Indé"
	const TABNEWS_5 = "tabnews5_urlpage";  // "Marvel"
	const TABNEWS_6 = "tabnews6_urlpage";  // "DCTrad+"
}
