<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2023 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\constant;

class comicsFormats
{
	const ISSUE = 1;
	const ONE_SHOT = 2;
	const ANNUAL = 3;
	const TPB = 4;
}
