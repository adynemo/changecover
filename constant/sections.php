<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\constant;

class sections
{
	const DC = 1;
	const MARVEL = 2;
	const INDE = 4;
	const HORSDC = 8;

	const SECTION_LOGO = [
		self::DC     => 'dc',
		self::MARVEL => 'marvel',
		self::INDE   => 'inde',
		self::HORSDC => 'dc',
	];
}
