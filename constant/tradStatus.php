<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\constant;

class tradStatus
{
	const WIP = 1;
	const PAUSED = 2;
	const ENDED = 3;
	const ABANDONED = 4;

	const WIP_LABEL = 'wip';
	const PAUSED_LABEL = 'paused';
	const ENDED_LABEL = 'ended';
	const ABANDONED_LABEL = 'abandoned';

	const STATUSES_MAP = [
		self::WIP       => self::WIP_LABEL,
		self::PAUSED    => self::PAUSED_LABEL,
		self::ENDED     => self::ENDED_LABEL,
		self::ABANDONED => self::ABANDONED_LABEL,
	];

	const STATUSES_PATH_MAP = [
		self::WIP_LABEL       => self::WIP,
		self::PAUSED_LABEL    => self::PAUSED,
		self::ENDED_LABEL     => self::ENDED,
		self::ABANDONED_LABEL => self::ABANDONED,
	];
}
