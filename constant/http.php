<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2023 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\constant;

class http
{
	public const GET = 'GET';
	public const POST = 'POST';
}
