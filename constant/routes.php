<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\constant;

class routes
{
	const COVERS_REQUEST = "ady_changecover_request";
	const NEWS_REQUEST = "ady_news_request";
	const NEWS_MANAGE = "ady_news_manage";
	const RELEASE_HOME = "ady_release_home_route";
	const RELEASE = "ady_release_route";
	const COMICS_INDEX = "ady_comics_index_route";
	const COMICS_INDEX_EDIT = "ady_edit_comics_index_route";
	const API_RELEASES_INTERVAL = "ady_api_releases_by_interval";
	const LOCG_NEWS = "ady_locg_news";
}
