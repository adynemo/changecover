<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\constant;

class paths
{
	public const STORE = "ext/ady/changecover/store/";
	public const LOGOS = self::STORE . "logos/";
	public const COVERS_PATH = 'images/ady/dctnews';
}
