<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'CHANGECOVER_JANUARY'      => 'janvier',
	'CHANGECOVER_FEBRUARY'     => 'février',
	'CHANGECOVER_MARCH'        => 'mars',
	'CHANGECOVER_APRIL'        => 'avril',
	'CHANGECOVER_MAY'          => 'mai',
	'CHANGECOVER_JUNE'         => 'juin',
	'CHANGECOVER_JULY'         => 'juillet',
	'CHANGECOVER_AUGUST'       => 'août',
	'CHANGECOVER_SEPTEMBER'    => 'septembre',
	'CHANGECOVER_OCTOBER'      => 'octobre',
	'CHANGECOVER_NOVEMBER'     => 'novembre',
	'CHANGECOVER_DECEMBER'     => 'décembre',
	'TN_SELECT'                => 'Tab News',
	'DCTNEWS_BACK_BUTTON'      => 'Retour',
	'DCTNEWS_BACK_HOME_BUTTON' => 'Retour à l’accueil',
	'DCTNEWS_UPLOAD_ERROR_NO_FILE' => 'Pas de cover transférée.',
	'DCTNEWS_UPLOAD_ERROR_TOO_BIG_FILE' => 'Le fichier est trop gros.',
	'DCTNEWS_UPLOAD_ERROR_FILE_ERROR' => 'Une erreur avec le fichier.',
	'DCTNEWS_UPLOAD_ERROR_EXT_ERROR' => 'Extension incorrecte.',
	'DCTNEWS_UPLOAD_ERROR_FAILURE' => 'Transfert échoué.',
	'DCTNEWS_UPLOAD_ERROR_SIZE_MISSING' => 'Impossible de lire les informations de taille.',
	'DCTNEWS_REQUEST_ERROR_SERIES' => 'Aucune série correspondante trouvée.',
	'DCTNEWS_REQUEST_ERROR_PUBLISH_POST_ENTITY_NOT_FOUND' => 'Une erreur s’est produite. Topic ou forum non trouvé.',
	'DCTNEWS_REQUEST_ERROR_PUBLISH_POST' => 'Une erreur s’est produite lors de la publication du post.',
));
