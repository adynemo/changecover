<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_CAT_DCT'                        => 'DC-Trad',
	'TN_ACP'                             => 'DC-Trad News Extension',
	'TN_TITLE'                           => 'Param&eacute;trage Extension DC-Trad News',
	'TN_STATUS'                          => 'Activer l’extension',
	'TN_EXPLAIN'                         => 'Configure Tab News dans le forum.',
	'TN_TYPE'                            => 'Tab News Slider',
	'TN_TYPE_EXPLAIN'                    => 'Tab News Slider Visible On/Off',
	'ACP_TITLE_TAB1'                     => 'Titre Onglet 1',
	'ACP_TITLE_TAB1_EXPLAIN'             => 'Entrez le titre pour l’onglet 1',
	'ACP_TN_CODE_TAB'                    => 'Titre onglet 2',
	'ACP_TN_CODE_TAB_EXPLAIN'            => 'Entrez le titre pour l’onglet 2.',
	'ACP_TITLE_TAB3'                     => 'Titre onglet 3',
	'ACP_TITLE_TAB3_EXPLAIN'             => 'Entrez le titre pour l’onglet 3',
	'ACP_TN_CODE_TAB4'                   => 'Titre onglet 4',
	'ACP_TN_CODE_TAB4_EXPLAIN'           => 'Entrez le titre pour l’onglet 4',
	'ACP_TITLE_TAB5'                     => 'Titre onglet 5',
	'ACP_TITLE_TAB5_EXPLAIN'             => 'Entrez le titre pour l’onglet 5',
	'ACP_TITLE_TAB6'                     => 'Titre onglet 6',
	'ACP_TITLE_TAB6_EXPLAIN'             => 'Entrez le titre pour l’onglet 6',
	'TN_FORUM7'                          => 'Tab News visible uniquement sur la page d’accueil ?',
	'ACP_TN_CODE'                        => 'Hauteur de Tab News',
	'ACP_TN_CODE_EXPLAIN'                => 'Entrez la hauteur en pixel (Exemple : 150).',
	'ACP_TABNEWS_URLPAGE'                => 'Entrez le code HTML pour l’onglet 1',
	'ACP_TABNEWS2_URLPAGE'               => 'Entrez le code HTML pour l’onglet 2',
	'ACP_TABNEWS_TEXT'                   => 'Entrez le code HTML pour l’onglet 3',
	'ACP_TABNEWS4_URLPAGE'               => 'Entrez le code HTML pour l’onglet 4',
	'ACP_TABNEWS5_URLPAGE'               => 'Entrez le code HTML pour l’onglet 5',
	'ACP_TABNEWS6_URLPAGE'               => 'Entrez le code HTML pour l’onglet 6',
	'ACP_TABNEWS2_URLPAGE_EXPLAIN'       => 'Code HTML',
	'ACP_TABNEWS_URLPAGE_EXPLAIN'        => 'Code HTML',
	'ACP_TABNEWS4_URLPAGE_EXPLAIN'       => 'Code HTML',
	'ACP_TABNEWS5_URLPAGE_EXPLAIN'       => 'Code HTML',
	'ACP_TABNEWS6_URLPAGE_EXPLAIN'       => 'Code HTML',
	'TN_FACEBOOKPAGE'                    => 'Activer l’onglet 1',
	'TN_FIRSTPOST'                       => 'Activer l’onglet 2',
	'ACP_ACTIVE_TAB3'                    => 'Activer l’onglet 3',
	'TN_FIRSTPOST4'                      => 'Activer l’onglet 4',
	'ACP_ACTIVE_TAB5'                    => 'Activer l’onglet 5',
	'ACP_ACTIVE_TAB6'                    => 'Activer l’onglet 6',
	'TABNEWS_TABNEWS_MOD'                => 'Tab News',
	'TN_CONFIG'                          => 'Configuration',
	'TN_SELECT'                          => 'Onglet',
	'TN_SAVED'                           => 'Sauvegarde les modifications.',
	'LOG_DCTNEWS_RELEASES_DISCORD_SEND'  => 'Les releases vont être publiées sur Discord',
	'LOG_DCTNEWS_RELEASES_DISCORD_FAILS' => 'La publication des releases sur Discord a échoué<br>» %1$s',
	'LOG_DCTNEWS_PUBLISH_FAILS' 		 => 'Demande de publication d’un comics échoué<br>» %1$s<br>» %2$s<br>» %3$s',
	'LOG_DCTNEWS_FETCH_USERS_ERROR'      => '<strong>[DCT-News] Impossible de récupérer les infos des utilisateurs</strong><br>» Requête : %1$s<br>» Exception : %2$s',
	'ACP_WEBHOOK'                        => 'Publication Discord',
	'ACP_WEBHOOK_EXPLAIN'                => 'Entrez l’URL du webhook Discord pour publier automatiquement les releases.',
));
