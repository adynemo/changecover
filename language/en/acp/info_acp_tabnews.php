<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_CAT_DCT'                        => 'DC-Trad',
	'TN_ACP'                             => 'DC-Trad News Extension',
	'TN_TITLE'                           => 'Tab News Extension Settings',
	'TN_STATUS'                          => 'Enable Extension',
	'TN_EXPLAIN'                         => 'Configure Tab News forum.',
	'TN_TYPE'                            => 'Tab News Slider',
	'TN_TYPE_EXPLAIN'                    => 'Show Tab News Slider On/Off',
	'ACP_TITLE_TAB1'                     => 'Title Tab 1',
	'ACP_TITLE_TAB1_EXPLAIN'             => 'Insert title for Tab 1',
	'ACP_TN_CODE_TAB'                    => 'Title tab 2',
	'ACP_TN_CODE_TAB_EXPLAIN'            => 'Enter the title for tab 2.',
	'ACP_TITLE_TAB3'                     => 'Title tab 3',
	'ACP_TITLE_TAB3_EXPLAIN'             => 'Insert title for tab 3',
	'ACP_TN_CODE_TAB4'                   => 'Title tab 4',
	'ACP_TN_CODE_TAB4_EXPLAIN'           => 'Insert title for tab 4',
	'ACP_TITLE_TAB5'                     => 'Title tab 5',
	'ACP_TITLE_TAB5_EXPLAIN'             => 'Insert title for tab 5',
	'ACP_TITLE_TAB6'                     => 'Title tab 6',
	'ACP_TITLE_TAB6_EXPLAIN'             => 'Insert title for tab 6',
	'TN_FORUM7'                          => 'View Tab News only in Home-Page?',
	'ACP_TN_CODE'                        => 'Height Tab News',
	'ACP_TN_CODE_EXPLAIN'                => 'Enter the height in pixel (Example 150).',
	'ACP_TABNEWS_URLPAGE'                => 'Insert code HTML for tab 1',
	'ACP_TABNEWS2_URLPAGE'               => 'Insert code HTML for tab 2',
	'ACP_TABNEWS_TEXT'                   => 'Insert code HTML for tab 3',
	'ACP_TABNEWS4_URLPAGE'               => 'Insert code HTML for tab 4',
	'ACP_TABNEWS5_URLPAGE'               => 'Insert code HTML for tab 5',
	'ACP_TABNEWS6_URLPAGE'               => 'Insert code HTML for tab 6',
	'ACP_TABNEWS2_URLPAGE_EXPLAIN'       => 'Code HTML',
	'ACP_TABNEWS_URLPAGE_EXPLAIN'        => 'Code HTML',
	'ACP_TABNEWS4_URLPAGE_EXPLAIN'       => 'Code HTML',
	'ACP_TABNEWS5_URLPAGE_EXPLAIN'       => 'Code HTML',
	'ACP_TABNEWS6_URLPAGE_EXPLAIN'       => 'Code HTML',
	'TN_FACEBOOKPAGE'                    => 'Active tab 1',
	'TN_FIRSTPOST'                       => 'Active tab 2',
	'ACP_ACTIVE_TAB3'                    => 'Active tab 3',
	'TN_FIRSTPOST4'                      => 'Active tab 4',
	'ACP_ACTIVE_TAB5'                    => 'Active tab 5',
	'ACP_ACTIVE_TAB6'                    => 'Active tab 6',
	'TABNEWS_TABNEWS_MOD'                => 'Tab News',
	'TN_CONFIG'                          => 'Settings',
	'TN_SELECT'                          => 'tab',
	'TN_SAVED'                           => 'Changes Saved.',
	'LOG_DCTNEWS_RELEASES_DISCORD_SEND'  => 'Releases will be published to Discord',
	'LOG_DCTNEWS_RELEASES_DISCORD_FAILS' => 'Publishing releases to Discord failed<br>» %1$s',
	'LOG_DCTNEWS_PUBLISH_FAILS' 		 => 'Comics publishing request failed<br>» %1$s',
	'LOG_DCTNEWS_FETCH_USERS_ERROR'      => '<strong>[DCT-News] Unable to fetch users data</strong><br>» Request: %1$s<br>» Exception : %2$s',
	'ACP_WEBHOOK'                        => 'Discord publishing',
	'ACP_WEBHOOK_EXPLAIN'                => 'Insert the Discord webhook URL to automatically publish releases',
));
