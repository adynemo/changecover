<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_CAT_DCTNEWS'                     => 'DC-Trad News',
	'ACL_U_CHANGECOVER_REQUESTER'         => 'Can request a header update.',
	'ACL_U_CHANGECOVER_APPROVER'          => 'Can approve a request for header update.',
	'ACL_A_CHANGECOVER_API'               => 'Can call API.',
	'ACL_U_CHANGECOVER_VIEW_HIDE_INDEX'   => 'Can view hiding element from comics index.',
	'ACL_A_CHANGECOVER_EDIT_INDEX'        => 'Can edit comics index.',
	'ACL_A_CHANGECOVER_TABNEWS_APPROVER'  => 'Can manage news',
	'ACL_U_CHANGECOVER_TABNEWS_REQUESTER' => 'Can submit a news',
));
