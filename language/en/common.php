<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'CHANGECOVER_JANUARY'      => 'january',
	'CHANGECOVER_FEBRUARY'     => 'february',
	'CHANGECOVER_MARCH'        => 'march',
	'CHANGECOVER_APRIL'        => 'april',
	'CHANGECOVER_MAY'          => 'may',
	'CHANGECOVER_JUNE'         => 'june',
	'CHANGECOVER_JULY'         => 'july',
	'CHANGECOVER_AUGUST'       => 'august',
	'CHANGECOVER_SEPTEMBER'    => 'september',
	'CHANGECOVER_OCTOBER'      => 'october',
	'CHANGECOVER_NOVEMBER'     => 'november',
	'CHANGECOVER_DECEMBER'     => 'december',
	'TN_SELECT'                => 'Tab News',
	'DCTNEWS_BACK_BUTTON'      => 'Back',
	'DCTNEWS_BACK_HOME_BUTTON' => 'Back to the home page',
	'DCTNEWS_UPLOAD_ERROR_NO_FILE' => 'No cover transmitted.',
	'DCTNEWS_UPLOAD_ERROR_TOO_BIG_FILE' => 'The file is too large.',
	'DCTNEWS_UPLOAD_ERROR_FILE_ERROR' => 'There is an issue with this file.',
	'DCTNEWS_UPLOAD_ERROR_EXT_ERROR' => 'Bad extension.',
	'DCTNEWS_UPLOAD_ERROR_FAILURE' => 'Transfer failed.',
	'DCTNEWS_UPLOAD_ERROR_SIZE_MISSING' => 'Unable to read the size information.',
	'DCTNEWS_REQUEST_ERROR_SERIES' => 'Related series not found.',
	'DCTNEWS_REQUEST_ERROR_PUBLISH_POST_ENTITY_NOT_FOUND' => 'Something went wrong. Topic or forum not found.',
	'DCTNEWS_REQUEST_ERROR_PUBLISH_POST' => 'Something went wrong on the post publishing.',
));
