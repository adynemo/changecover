<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\event;

use ady\changecover\constant\permissions;
use ady\changecover\core\access;
use phpbb\config\config;
use phpbb\config\db_text;
use phpbb\event\data;
use phpbb\template\template;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class listener implements EventSubscriberInterface
{
	private access $accessor;
	private db_text $config_text;
	private template $template;
	private config $config;

	static public function getSubscribedEvents()
	{
		return [
			'core.permissions' => 'permissions',
			'core.page_header' => 'add_page_header_link',
			'core.user_setup'  => 'load_language_on_setup',
			'core.common'      => 'common_setup',
		];
	}

	public function __construct(
		access $accessor,
		db_text $config_text,
		template $template,
		config $config
	)
	{
		$this->accessor = $accessor;
		$this->config_text = $config_text;
		$this->template = $template;
		$this->config = $config;
	}

	public function permissions(data $event)
	{
		$permission_categories = [
			'ady_dct' => [
				permissions::U_NEWS_REQUESTER,
				permissions::A_NEWS_MANAGER,
				permissions::U_COVER_REQUESTER,
				permissions::U_COVER_APPROVER,
				permissions::U_CAN_VIEW_HIDE_INDEX,
				permissions::A_EDIT_INDEX,
				permissions::A_API,
			],
		];

		$changecover_permissions = [];

		foreach ($permission_categories as $cat => $permissions)
		{
			foreach ($permissions as $permission)
			{
				$changecover_permissions[$permission] = [
					'lang' => 'ACL_' . strtoupper($permission),
					'cat'  => $cat,
				];
			}
		}

		$event['permissions'] = array_merge($event['permissions'], $changecover_permissions);
		$event['categories'] = array_merge($event['categories'], ['ady_dct' => 'ACP_CAT_DCT']);
	}

	public function add_page_header_link()
	{
		$this->accessor->assign_access();
	}

	public function common_setup($event)
	{
		$tabnews_data = $this->config_text->get_array([
			'tabnews_text',
			'tabnews_urlpage',
			'tabnews2_urlpage',
			'tabnews4_urlpage',
			'tabnews5_urlpage',
			'tabnews6_urlpage',
		]);

		$this->template->assign_vars([
			'S_TN_STATUS'       => $this->config['tn_status'],
			'S_TN_TYPE'         => $this->config['tn_type'],
			'S_TN_FIRSTPOST'    => $this->config['tn_firstpost'],
			'S_TN_FIRSTPOST4'   => $this->config['tn_firstpost4'],
			'ACTIVE_TAB3'       => $this->config['active_tab3'],
			'S_TN_DIGG'         => $this->config['tn_digg'],
			'S_TN_FORUM7'       => $this->config['tn_forum7'],
			'S_TN_FACEBOOKPAGE' => $this->config['tn_facebookpage'],
			'ACTIVE_TAB5'       => $this->config['active_tab5'],
			'ACTIVE_TAB6'       => $this->config['active_tab6'],
			'TN_CODE'           => (isset($this->config['tabnews_tn_code'])) ? $this->config['tabnews_tn_code'] : '',
			'TITLE_TAB1'        => (isset($this->config['title_tab1'])) ? $this->config['title_tab1'] : '',
			'TITLE_TAB3'        => (isset($this->config['title_tab3'])) ? $this->config['title_tab3'] : '',
			'TN_CODE_TAB'       => (isset($this->config['tabnews_tn_code_tab'])) ? $this->config['tabnews_tn_code_tab'] : '',
			'TN_CODE_TAB4'      => (isset($this->config['tabnews_tn_code_tab4'])) ? $this->config['tabnews_tn_code_tab4'] : '',
			'TITLE_TAB5'        => (isset($this->config['title_tab5'])) ? $this->config['title_tab5'] : '',
			'TITLE_TAB6'        => (isset($this->config['title_tab6'])) ? $this->config['title_tab6'] : '',
			'TABNEWS_URLPAGE'   => htmlspecialchars_decode($tabnews_data['tabnews_urlpage']),
			'TABNEWS2_URLPAGE'  => htmlspecialchars_decode($tabnews_data['tabnews2_urlpage']),
			'TABNEWS4_URLPAGE'  => htmlspecialchars_decode($tabnews_data['tabnews4_urlpage']),
			'TABNEWS_TEXT'      => htmlspecialchars_decode($tabnews_data['tabnews_text']),
			'TABNEWS5_URLPAGE'  => htmlspecialchars_decode($tabnews_data['tabnews5_urlpage']),
			'TABNEWS6_URLPAGE'  => htmlspecialchars_decode($tabnews_data['tabnews6_urlpage']),
		]);
	}

	public function load_language_on_setup(data $event)
	{
		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = [
			'ext_name' => 'ady/changecover',
			'lang_set' => 'common',
		];
		$event['lang_set_ext'] = $lang_set_ext;
	}
}
