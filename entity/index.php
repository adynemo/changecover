<?php

namespace ady\changecover\entity;

use ady\changecover\constant\tables;
use ady\changecover\constant\tradStatus;

class index
{
    private int $id;
    private string $title;
    private string $sortedTitle;
    private string $sortChar;
    private string $url;
    private bool $hidden;
    private int $status;
    private ?string $editor;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): index
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): index
    {
        $this->title = $title;

        return $this;
    }

    public function getSortedTitle(): string
    {
        return $this->sortedTitle;
    }

    public function setSortedTitle(string $sortedTitle): index
    {
        $this->sortedTitle = $sortedTitle;

        return $this;
    }

    public function getSortChar(): string
    {
        return $this->sortChar;
    }

    public function setSortChar(string $sortChar): index
    {
        $this->sortChar = $sortChar;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): index
    {
        $this->url = $url;

        return $this;
    }

    public function isHidden(): bool
    {
        return $this->hidden;
    }

    public function setHidden(bool $hidden): index
    {
        $this->hidden = $hidden;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): index
    {
        $map = [
            tradStatus::WIP,
            tradStatus::PAUSED,
            tradStatus::ENDED,
            tradStatus::ABANDONED,
        ];

        if (!isset($map[$status]))
        {
            throw new \Exception(tradStatus::class . ' is required');
        }
        
        $this->status = $status;

        return $this;
    }

    public function getEditor(): ?string
    {
        return $this->editor;
    }

    public function setEditor(?string $editor): index
    {
        $this->editor = $editor;

        return $this;
    }

    public function toDatabase(): array
    {
		$keys = tables::COMICS_INDEX_COLUMNS;
        $keys = array_flip($keys);
        unset($keys['id']);
        $keys = array_flip($keys);

		return array_combine(
            $keys,
            [
                $this->title,
                $this->sortedTitle,
                $this->sortChar,
                $this->url,
                $this->hidden,
                $this->status,
                $this->editor,
            ]
        );
    }
}