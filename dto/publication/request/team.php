<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2023 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\dto\publication\request;

class team
{
	public array $trad = [];
	public array $c1 = [];
	public array $edit = [];
	public array $c2 = [];

	public function toArray(): array
	{
		return [
			'trad' => $this->trad,
			'c1'   => $this->c1,
			'edit' => $this->edit,
			'c2'   => $this->c2,
		];
	}
}
