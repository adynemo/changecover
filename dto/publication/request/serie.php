<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2023 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\dto\publication\request;

// todo: rename series
class serie
{
	public ?int $id;
	public ?string $title;
	public ?int $forum_id;
	public ?string $share_link;
	public int $status;
	public ?string $url = null;
}
