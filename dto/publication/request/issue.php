<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2023 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\dto\publication\request;

class issue
{
	public ?string $number;
	public ?string $title;
	public int $format;
	public string $cover;
}
