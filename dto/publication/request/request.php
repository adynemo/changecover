<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2023 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\dto\publication\request;

class request
{
	public bool $is_first_issue;
	public bool $publish_post;
	public serie $serie;
	public issue $issue;
	public team $team;
	public int $editor;
	public string $description;

	public function __construct()
	{
		$this->serie = new serie();
		$this->issue = new issue();
		$this->team = new team();
	}
}
