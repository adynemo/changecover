<?php

namespace ady\changecover\dto\publication;

class publication
{
    public int $editor;
    public string $url_release;
    public string $path_cover;
    public int $user_id;
    public string $serie_title;
    public ?string $issue_title;
    public ?string $issue_number;
    public ?string $comics_format;
    public bool $comics_index;
    public int $status;
}
