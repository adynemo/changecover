<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\controller;

use ady\changecover\constant\permissions;
use ady\changecover\constant\routes;
use ady\changecover\core\core;
use ady\changecover\core\news as NewsCore;
use phpbb\auth\auth;
use phpbb\controller\helper;
use phpbb\exception\http_exception;
use phpbb\request\request;
use phpbb\template\template;
use phpbb\user;
use Symfony\Component\HttpFoundation\Response;

class news
{
	const ERROR_NAME = "NEWS";

	/* @var core */
	protected $ady_core;
	/* @var news */
	protected $ady_core_news;
	/* @var helper */
	protected $helper;
	/* @var template */
	protected $template;
	/* @var user */
	protected $user;
	/* @var request */
	protected $request;
	/* @var auth */
	protected $auth;

	public function __construct(
		core $ady_core,
		NewsCore $ady_core_news,
		helper $helper,
		template $template,
		user $user,
		auth $auth,
		request $request
	)
	{
		$this->ady_core = $ady_core;
		$this->ady_core_news = $ady_core_news;
		$this->helper = $helper;
		$this->template = $template;
		$this->user = $user;
		$this->auth = $auth;
		$this->request = $request;
	}

	public function request(): Response
	{
		$this->ady_core->checkAuth(permissions::U_NEWS_REQUESTER, $this->user);

		$submit = $this->request->is_set_post('post');

		if (!$submit)
		{
			return $this->helper->render('tab_news_request.html', "News Requester");
		}

		$request = $this->request->variable('tabnews', ['' => ''], true);

		try {
			$file = $this->request->file('image');
			$request['image'] = $this->ady_core_news->upload_image($file);
		} catch (\Throwable $exception) {
			$this->template->assign_vars([
				"ERROR" => 1,
				"CODE"  => self::ERROR_NAME,
				"MESSAGE" => $exception->getMessage(),
			]);

			return $this->helper->render('error.html', "Header Error");
		}

		$request['user_id'] = $this->user->data['user_id'];
		$request['created_at'] = (new \DateTime('now'))->getTimestamp();

		if (!$this->ady_core_news->save($request))
		{
			$this->template->assign_vars([
				"ERROR" => 2,
				"CODE"  => self::ERROR_NAME,
			]);

			return $this->helper->render('error.html', "Header Error");
		}

		$this->template->assign_vars([
			"SUCCESS"  => "sent",
			"CALLBACK" => $this->helper->route(routes::NEWS_REQUEST),
		]);

		return $this->helper->render('success.html', "Header Success");
	}

	public function manage(): Response
	{
		$this->ady_core->checkAuth(permissions::U_NEWS_REQUESTER, $this->user);

		$submit = $this->request->is_set_post('post');
		$remove = $this->request->is_set_post('remove');

		if ($submit || $remove)
		{
			$this->ady_core->checkAuth(permissions::A_NEWS_MANAGER, $this->user);
			$id = $this->request->variable('news', 0);
			$news = $this->ady_core_news->find_one($id);

			if ($remove && [] !== $news)
			{
				try
				{
					$this->ady_core_news->remove($news);
				}
				catch (\Throwable $exception)
				{
					// explicitly ignored for now (error on image removal)
				}

				$this->template->assign_vars([
					'MESSAGE'  => 'La news a bien été supprimée.',
					'CALLBACK' => $this->helper->route(routes::NEWS_MANAGE),
				]);

				return $this->helper->render('new_success.html', "Success");
			}

			if ($this->ady_core_news->publish($news))
			{
				$this->template->assign_vars([
					'MESSAGE'  => 'La news a bien été publiée.',
					'CALLBACK' => $this->helper->route(routes::NEWS_MANAGE),
				]);

				return $this->helper->render('new_success.html', "Header Success");
			}

			$this->template->assign_vars([
				"ERROR" => 3,
				"CODE"  => self::ERROR_NAME,
			]);

			return $this->helper->render('error.html', "Header Error");
		}

		$this->template->assign_vars([
			"NEWS"           => $this->ady_core_news->find(),
			"A_NEWS_MANAGER" => $this->auth->acl_get(permissions::A_NEWS_MANAGER),
		]);

		return $this->helper->render('tab_news_manage.html', "News management");
	}

	public function edit(string $id): Response
	{
		$this->ady_core->checkAuth(permissions::A_NEWS_MANAGER, $this->user);

		$news = $this->ady_core_news->find_one($id);
		if ([] === $news)
		{
			throw new http_exception(404, 'PAGE_NOT_FOUND');
		}

		$submit = $this->request->is_set_post('post');
		if ($submit)
		{
			$request = $this->request->variable('tabnews', ['' => ''], true);
			$file = $this->request->file('image');

			if (UPLOAD_ERR_NO_FILE != $file['error'])
			{
				try {
					$request['image'] = $this->ady_core_news->upload_image($file);
				} catch (\Throwable $exception) {
					$this->template->assign_vars([
						"ERROR" => 4,
						"CODE"  => self::ERROR_NAME,
						"MESSAGE" => $exception->getMessage(),
					]);

					return $this->helper->render('error.html', "Header Error");
				}
			}

			if (!$this->ady_core_news->update($request, $news))
			{
				$this->template->assign_vars([
					"ERROR" => 5,
					"CODE"  => self::ERROR_NAME,
				]);

				return $this->helper->render('error.html', "Header Error");
			}

			$this->template->assign_vars([
				"SUCCESS"  => "sent",
				"CALLBACK" => $this->helper->route(routes::NEWS_MANAGE),
			]);

			return $this->helper->render('success.html', "Header Success");
		}

		$this->template->assign_vars([
			"NEWS" => $news,
		]);

		return $this->helper->render('tab_news_request.html', "News edit");
	}
}
