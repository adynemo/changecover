<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\controller;

use ady\changecover\constant\routes;
use ady\changecover\core\core;
use ady\changecover\core\releases as releasesCore;
use phpbb\auth\auth;
use phpbb\controller\helper;
use phpbb\template\template;
use phpbb\user;
use Symfony\Component\HttpFoundation\Response;

class releases
{
	/* @var core */
	protected $ady_core;

	/* @var releases */
	protected $ady_releases;

	/* @var helper */
	protected $helper;

	/* @var template */
	protected $template;

	/* @var user */
	protected $user;

	/* @var auth */
	protected $auth;

	public function __construct(
		core $ady_core,
		releasesCore $ady_releases,
		helper $helper,
		template $template,
		user $user,
		auth $auth
	)
	{
		$this->ady_core = $ady_core;
		$this->ady_releases = $ady_releases;
		$this->helper = $helper;
		$this->template = $template;
		$this->user = $user;
		$this->auth = $auth;
	}

	public function releases_home(): Response
	{
		$all_releases = $this->ady_releases->fetch_all();
		$years = [];

		foreach ($all_releases as $year => $releases)
		{
			$years[$year] = $releases;
		}
		unset($all_releases);
		krsort($years);

		$this->template->assign_vars([
			"HOME"  => true,
			"YEARS" => $years,
			"ROUTE" => $this->helper->route(routes::RELEASE),
		]);

		return $this->helper->render('releases.html', "Releases home");
	}

	public function releases(int $year, int $month): Response
	{
		$request_date = (new \DateTime())->setDate($year, $month, 1);
		$actual_date = new \DateTime();

		$previous_month = (new \DateTime())
			->setDate($year, $month, 1)
			->modify('1 month ago');

		$last_releases = [
			'year'  => $previous_month->format('Y'),
			'month' => $previous_month->format('m'),
		];

		$this->template->assign_vars(["LAST" => $this->helper->route(routes::RELEASE, $last_releases)]);

		if ($request_date->format('Ym') < $actual_date->format('Ym'))
		{
			$next_month = (new \DateTime())
				->setDate($year, $month, 1)
				->modify('+1 month');

			$next_releases = [
				'year'  => $next_month->format('Y'),
				'month' => $next_month->format('m'),
			];

			$this->template->assign_vars([
				"NEXT" => $this->helper->route(routes::RELEASE, $next_releases),
			]);
		}

		$releases = $this->ady_releases->fetch_by_date($year, $month);
		$count = $releases['count'];
		unset($releases['count']);

		$this->template->assign_vars([
			"ALL_RELEASES" => $releases,
			"DATE"         => $request_date->format('m/Y'),
			"ROUTE"        => $this->helper->route(routes::RELEASE),
			"COUNT"        => $count,
		]);

		return $this->helper->render('releases.html', 'Releases ' . $request_date->format('m/Y'));
	}

	public function go_releases()
	{
		$now = new \DateTime();
		$url = $this->helper->route(routes::RELEASE, [
			'year'  => $now->format('Y'),
			'month' => $now->format('m'),
		]);

		redirect($url);
	}
}
