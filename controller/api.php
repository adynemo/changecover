<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\controller;

use ady\changecover\constant\http;
use ady\changecover\constant\permissions;
use ady\changecover\constant\tokenName;
use ady\changecover\core\core;
use ady\changecover\core\indexComics;
use ady\changecover\core\releases;
use ady\changecover\core\users;
use phpbb\auth\auth;
use phpbb\log\log;
use phpbb\request\request_interface;
use phpbb\user;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class api
{
	private releases $ady_releases;
	private indexComics $ady_comics;
	private user $user;
	private auth $auth;
	private request_interface $request;
	private log $log;
	private users $users;
	private core $ady_core;

	public function __construct(
		releases $ady_releases,
		user $user,
		auth $auth,
		request_interface $request,
		indexComics $comics,
		log $log,
		users $users,
		core $ady_core
	)
	{
		$this->ady_releases = $ady_releases;
		$this->ady_comics = $comics;
		$this->user = $user;
		$this->auth = $auth;
		$this->request = $request;
		$this->log = $log;
		$this->users = $users;
		$this->ady_core = $ady_core;
	}

	public function releases_by_interval($begin_date, $end_date)
	{
		$this->request->header('Content-Type: application/json');

		if (!$this->auth->acl_get(permissions::A_API))
		{
			if (!$this->user->data['is_registered'])
			{
				$user = $this->request->server('PHP_AUTH_USER');
				$pw = $this->request->server('PHP_AUTH_PW');
				if (!empty($user) && !empty($pw))
				{
					$this->auth->login($user, $pw);
				}
			}

			http_response_code(403);
			exit;
		}

		$this->ady_core->log_visit();

		try
		{
			$begin_date = new \DateTime($begin_date);
			$end_date = new \DateTime($end_date);
		}
		catch (\Throwable $exception)
		{
			http_response_code(500);
			exit;
		}

		$releases = $this->ady_releases->fetch_by_interval($begin_date, $end_date);

		echo json_encode($releases, JSON_PRETTY_PRINT);
		exit;
	}

	public function search_series(): Response
	{
		$response = $this->allow_request(tokenName::SEARCH);
		if (null !== $response)
		{
			return $response;
		}

		$query = $this->request->variable('q', '', true);
		if (3 > strlen($query))
		{
			return new JsonResponse([]);
		}

		try
		{
			$entries = $this->ady_comics->find_entry_by_query($query);
		}
		catch (\Throwable $exception)
		{
			$this->log->add(
				'critical',
				$this->user->data['user_id'],
				$this->user->ip,
				'LOG_PEPPER_FETCH_USERS_ERROR',
				false,
				[$query, (string) $exception]
			);
			return new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
		}

		$results = [];
		foreach ($entries as $entry)
		{
			$results[] = [
				'id'     => $entry['id'],
				'title'  => $entry['title'],
				'url'    => $entry['url'],
				'status' => $entry['status'],
				'editor' => $entry['editor'],
			];
		}

		return new JsonResponse($results);
	}

	public function search_users(): Response
	{
		$response = $this->allow_request(tokenName::SEARCH);
		if (null !== $response)
		{
			return $response;
		}

		$query = $this->request->variable('q', '', true);

		if ('' === $query)
		{
			return new JsonResponse([]);
		}

		try
		{
			$users = $this->users->find_users_by_query(strtolower($query));
		}
		catch (\Throwable $exception)
		{
			$this->log->add(
				'critical',
				$this->user->data['user_id'],
				$this->user->ip,
				'LOG_DCTNEWS_FETCH_USERS_ERROR',
				false,
				[$query, (string) $exception]
			);
			return new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
		}

		$light_users = [];
		foreach ($users as $user)
		{
			$light_users[] = [
				'id'       => $user['user_id'],
				'avatar'   => $user['avatar'],
				'username' => $user['username'],
			];
		}

		return new JsonResponse($light_users);
	}

	private function allow_request(string $token = null): ?JsonResponse
	{
		if (!$this->user->data['is_registered'])
		{
			return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
		}
		if (!$this->auth->acl_get(permissions::U_COVER_REQUESTER))
		{
			return new JsonResponse(null, Response::HTTP_FORBIDDEN);
		}
		if (!$this->request->is_ajax())
		{
			return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
		}
		if (http::POST !== $this->request->server('REQUEST_METHOD'))
		{
			return new JsonResponse(null, Response::HTTP_METHOD_NOT_ALLOWED);
		}
		if (false === check_form_key($token))
		{
			return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
		}

		return null;
	}
}
