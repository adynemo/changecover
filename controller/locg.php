<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\controller;

use ady\changecover\core\core;
use ady\changecover\core\locg as LOCGCore;
use phpbb\controller\helper;
use phpbb\log\log;
use phpbb\path_helper;
use phpbb\request\request;
use phpbb\template\template;
use phpbb\user;

class locg
{
	protected LOCGCore $ady_locg;
	protected helper $helper;
	protected template $template;
	protected user $user;
	protected request $request;
	protected path_helper $path_helper;
	protected core $ady_core;

	public function __construct(
		LOCGCore $ady_locg,
		helper $helper,
		template $template,
		user $user,
		request $request,
		path_helper $path_helper,
		core $ady_core
	)
	{
		$this->ady_locg = $ady_locg;
		$this->helper = $helper;
		$this->template = $template;
		$this->user = $user;
		$this->request = $request;
		$this->path_helper = $path_helper;
		$this->ady_core = $ady_core;
	}

	public function locg_news()
	{
		if (!$this->user->data['is_registered'])
		{
			login_box();
		}

		$this->ady_core->log_visit();

		$section = $this->request->variable('section', ['' => ''], true);
		$filters = [];
		if ([] !== $section)
		{
			if (!isset($section['dc']))
			{
				$filters[] = 'publisher_exclude[]=1';
			}
			if (!isset($section['marvel']))
			{
				$filters[] = 'publisher_exclude[]=2';
			}
			if (!isset($section['inde']))
			{
				if (isset($section['dc']))
				{
					$filters[] = 'publisher[]=1';
				}
				if (isset($section['marvel']))
				{
					$filters[] = 'publisher[]=2';
				}
			}
		}

		$year = $this->request->variable('year', 0, true);
		$month = $this->request->variable('month', 0, true);
		$week = $this->request->variable('week', false, true);

		$date = (new \DateTime())->modify('first day of');
		if (0 < $year && 0 < $month)
		{
			$date = (new \DateTime())->setDate($year, $month, 1);
		}
		$date_string = $date->format('Y-m-d');

		$news = $this->ady_locg->fetch($date, $filters, (false === $week) ? LOCGCore::PREVIEWS_TYPE : LOCGCore::WEEK_TYPE);

		$this->template->assign_vars([
			'date'     => $date_string,
			'week'     => $week,
			'section'  => $section,
			'news'     => $news,
			'no_cover' => $this->get_default_image_path(),
		]);

		return $this->helper->render('locg_news.html', "All #1's");
	}

	private function get_default_image_path(): string
	{
		$board_url = generate_board_url() . '/';
		$corrected_path = $this->path_helper->get_web_root_path();
		$web_path = (defined('PHPBB_USE_BOARD_URL_PATH') && PHPBB_USE_BOARD_URL_PATH) ? $board_url : $corrected_path;

		return sprintf('%sext/ady/changecover/store/%s', $web_path, 'no-cover-med.jpg');
	}
}
