<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\controller;

use ady\changecover\constant\permissions;
use ady\changecover\constant\routes;
use ady\changecover\constant\tradStatus;
use ady\changecover\core\core;
use ady\changecover\core\indexComics as IndexComicsCore;
use phpbb\auth\auth;
use phpbb\controller\helper;
use phpbb\exception\http_exception;
use phpbb\request\request;
use phpbb\template\template;
use phpbb\user;
use Symfony\Component\HttpFoundation\Response;

class indexComics
{
	private const CREATE_ACTION = 'creation';
	private const EDIT_ACTION = 'edition';

	/* @var core */
	protected $ady_core;

	/* @var indexComics */
	protected $ady_indexcomics;

	/* @var helper */
	protected $helper;

	/* @var template */
	protected $template;

	/* @var user */
	protected $user;

	/* @var request */
	protected $request;

	/** \phpbb\auth */
	protected $auth;

	public function __construct(
		core $ady_core,
		IndexComicsCore $ady_indexcomics,
		helper $helper,
		template $template,
		user $user,
		auth $auth,
		request $request
	)
	{
		$this->ady_core = $ady_core;
		$this->ady_indexcomics = $ady_indexcomics;
		$this->helper = $helper;
		$this->template = $template;
		$this->user = $user;
		$this->auth = $auth;
		$this->request = $request;
	}

	public function comics_index(string $character): Response
	{
		$alph = range('a', 'z');
		$alph[] = 'other';

		if (!$character)
		{
			$this->template->assign_vars([
				'CHARACTERS'               => $alph,
				'HOME'                     => true,
				'A_CHANGECOVER_EDIT_INDEX' => $this->auth->acl_get(permissions::A_EDIT_INDEX),
			]);

			return $this->helper->render('comics_index.html', 'Index Comics');
		}
		if (!in_array($character, $alph))
		{
			throw new http_exception(404, 'PAGE_NOT_FOUND');
		}

		$character_todb = $character === 'other' ? '#' : $character;
		$index_page = $this->ady_indexcomics->fetch_by_character($character_todb);

		if (!$index_page)
		{
			throw new http_exception(404, 'PAGE_NOT_FOUND');
		}

		if ($character !== 'a')
		{
			$i = array_search($character, $alph);
			$this->template->assign_vars([
				'LAST' => $this->helper->route(routes::COMICS_INDEX, ['character' => $alph[$i - 1]]),
			]);
		}
		if ($character !== 'other')
		{
			$i = array_search($character, $alph);
			$this->template->assign_vars([
				'NEXT' => $this->helper->route(routes::COMICS_INDEX, ['character' => $alph[$i + 1]]),
			]);
		}

		$this->template->assign_vars([
			'U_CHANGECOVER_VIEW_HIDE_INDEX' => $this->auth->acl_get(permissions::U_CAN_VIEW_HIDE_INDEX),
			'A_CHANGECOVER_EDIT_INDEX'      => $this->auth->acl_get(permissions::A_EDIT_INDEX),
			'CHARACTER'                     => $character,
			'ROUTE'                         => $this->helper->route(routes::COMICS_INDEX),
			'ROWS'                          => $index_page,
			'CHARACTERS'                    => $alph,
		]);

		return $this->helper->render('comics_index.html', 'Index Comics');
	}

	public function edit(int $id): Response
	{
		$this->ady_core->checkAuth(permissions::A_EDIT_INDEX, $this->user);

		$submit = $this->request->is_set_post('post');
		$remove = $this->request->is_set_post('remove');
		$element = $this->ady_indexcomics->fetch_one($id);

		if (!$element)
		{
			throw new http_exception(404, 'PAGE_NOT_FOUND');
		}
		else
		{
			if ($submit)
			{
				$elementSubmitted = $this->request->variable('element', ['' => ''], true);
				$element = array_merge($element, $elementSubmitted);
				$element['hide'] = isset($elementSubmitted['hide']);
				unset($elementSubmitted);

				$this->ady_indexcomics->update($id, $element);

				$this->template->assign_vars([
					'SUCCESS' => true,
				]);
			}
			else if ($remove)
			{
				$this->ady_indexcomics->delete($id);

				$this->template->assign_vars([
					'SUCCESS' => true,
					'REMOVED' => true,
				]);
			}
			else
			{
				$this->template->assign_vars([
					'ACTION'  => self::EDIT_ACTION,
					'ELEMENT' => $element,
				]);
			}

			return $this->helper->render('edit_comics_index.html', 'Edit Index Comics');
		}
	}

	public function create(): Response
	{
		$this->ady_core->checkAuth(permissions::A_EDIT_INDEX, $this->user);

		$submit = $this->request->is_set_post('post');

		if ($submit)
		{
			$element = $this->request->variable('element', ['' => ''], true);
			$element['hide'] = isset($element['hide']);

			$this->ady_indexcomics->create($element);

			$this->template->assign_vars([
				'SUCCESS' => true,
				'CREATED' => true,
			]);
		}
		else
		{
			$this->template->assign_vars([
				'ACTION' => self::CREATE_ACTION,
			]);
		}

		return $this->helper->render('edit_comics_index.html', 'Edit Index Comics');
	}

	public function display_comics_by_status(string $status): Response
	{
		if (!array_key_exists($status, tradStatus::STATUSES_PATH_MAP))
		{
			throw new http_exception(404, 'PAGE_NOT_FOUND');
		}

		$comics = $this->ady_indexcomics->fetch_by_status(tradStatus::STATUSES_PATH_MAP[$status]);

		$this->template->assign_vars([
			'ROWS'                     => $comics,
			'STATUS'                   => tradStatus::STATUSES_PATH_MAP[$status],
			'A_CHANGECOVER_EDIT_INDEX' => $this->auth->acl_get(permissions::A_EDIT_INDEX),
		]);

		return $this->helper->render('comics_by_status.html', 'Comics en pause');
	}
}
