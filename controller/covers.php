<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\controller;

use ady\changecover\constant\permissions;
use ady\changecover\constant\routes;
use ady\changecover\constant\sections;
use ady\changecover\constant\tokenName;
use ady\changecover\core\access;
use ady\changecover\core\core;
use ady\changecover\core\covers as CoversCore;
use ady\changecover\core\discordPublisher;
use ady\changecover\core\indexComics;
use ady\changecover\core\publish;
use ady\changecover\core\releases;
use ady\changecover\dto\publication\publication;
use ady\changecover\dto\publication\request\request as pub_request;
use phpbb\controller\helper;
use phpbb\language\language;
use phpbb\log\log;
use phpbb\request\request;
use phpbb\template\template;
use phpbb\user;
use Symfony\Component\HttpFoundation\Response;

class covers
{
	private const ERROR_NAME = 'APR';

	private const SUCCESS_PAGE_TITLE = 'Header Success';
	private const ERROR_PAGE_TITLE = 'Header Error';
	private const ADMIN_PAGE_TITLE = 'Header Admin';
	private const TEAM_PAGE_TITLE = 'Header Team';
	private const HOME_PAGE_TITLE = 'Header Home';

	private core $ady_core;
	private CoversCore $ady_covers;
	private indexComics $ady_indexcomics;
	private releases $ady_releases;
	private publish $ady_publish;
	private discordPublisher $publisher;
	private helper $helper;
	private template $template;
	private user $user;
	private request $request;
	private log $log;
	private access $accessor;
	private language $lang;

	public function __construct(
		core $ady_core,
		CoversCore $ady_covers,
		indexComics $ady_indexcomics,
		releases $ady_releases,
		publish $ady_publish,
		discordPublisher $publisher,
		helper $helper,
		template $template,
		user $user,
		request $request,
		log $log,
		access $accessor,
		language $lang
	)
	{
		$this->ady_core = $ady_core;
		$this->ady_covers = $ady_covers;
		$this->ady_indexcomics = $ady_indexcomics;
		$this->ady_releases = $ady_releases;
		$this->ady_publish = $ady_publish;
		$this->publisher = $publisher;
		$this->helper = $helper;
		$this->template = $template;
		$this->user = $user;
		$this->request = $request;
		$this->log = $log;
		$this->accessor = $accessor;
		$this->lang = $lang;
	}

	public function home(): Response
	{
		if (!$this->user->data['is_registered'])
		{
			login_box();
		}

		$this->accessor->assign_access();

		return $this->helper->render('home.html', self::HOME_PAGE_TITLE);
	}

	public function request(): Response
	{
		$this->ady_core->checkAuth(permissions::U_COVER_REQUESTER, $this->user);
		$submit = $this->request->is_set_post('post');
		$v2 = (bool) $this->request->variable('v2', false);

		if ($submit)
		{
			try {
				$file = $this->request->file('cover');
				$cover = $this->ady_covers->upload_cover($file);
				$publication = (true === $v2) ? $this->process_v2($cover) : $this->process_v1($cover);
			} catch (\Throwable $exception) {
				$this->log->add(
					'critical',
					$this->user->data['user_id'],
					$this->user->ip,
					'LOG_DCTNEWS_PUBLISH_FAILS',
					false,
					[$this->lang->lang($exception->getMessage()), $exception->getCode(), $exception->getTraceAsString()]
				);
				$this->template->assign_vars([
					"ERROR" => 1,
					"CODE"  => self::ERROR_NAME,
					"MESSAGE" => $exception->getMessage(),
				]);

				return $this->helper->render('error.html', self::ERROR_PAGE_TITLE);
			}

			$result = false;
			$exception = null;
			try {
				$result = $this->ady_covers->save_request($publication);
			} catch (\Throwable $exception) {
				
			}
			if (false === $result)
			{
				$this->log->add(
					'critical',
					$this->user->data['user_id'],
					$this->user->ip,
					'LOG_DCTNEWS_PUBLISH_FAILS',
					false,
					[$exception ? $exception->getMessage() : '']
				);
				$this->template->assign_vars([
					"ERROR" => 2,
					"CODE"  => self::ERROR_NAME,
					"MESSAGE" => "Impossible d'enregistrer la demande.",
				]);

				return $this->helper->render('error.html', self::ERROR_PAGE_TITLE);
			}

			$this->template->assign_vars([
				"SUCCESS"  => "sent",
				"CALLBACK" => $this->helper->route(routes::COVERS_REQUEST, ['v2' => $v2]),
			]);

			return $this->helper->render('success.html', self::SUCCESS_PAGE_TITLE);
		}
		else
		{
			$this->lang->add_lang('posting');
			$this->template->assign_vars([
				"FORUM_SELECT" => $this->ady_publish->build_forum_select(),
				"S_BBCODE_ALLOWED" => $this->accessor->get_config('allow_bbcode'),
				"S_LINKS_ALLOWED" => $this->accessor->get_config('allow_post_links'),
				"S_BBCODE_QUOTE" => true,
				"S_BBCODE_IMG" => true,
			]);
		}

		$this->accessor->set_csrf_token(tokenName::SEARCH);

		$template = $v2 ? 'request-v2.html' : 'request.html';

		return $this->helper->render($template, self::TEAM_PAGE_TITLE);
	}

	public function approve(): Response
	{
		$this->ady_core->checkAuth(permissions::U_COVER_APPROVER, $this->user);
		$submit = $this->request->is_set_post('post');
		$to_approve = $this->ady_covers->fetch_cover_to_approve();

		if ($submit)
		{
			$error = 0;
			$approve = $this->request->variable('approve', [0 => 0], true);
			$index = $this->request->variable('index', [0 => 0], true);

			$ids = array_map(function ($cover) {
				return $cover['id'];
			}, $to_approve);

			$remove = array_diff($ids, $approve);
			$index = array_diff($index, $remove);

			if ([] !== $index)
			{
				$this->ady_indexcomics->insert($index);
			}

			if ([] !== $remove)
			{
				$files = $this->fetch_covers($remove);
				$removed = $this->ady_covers->delete_request($remove);

				if (!$removed)
				{
					$error += 1;
				}
				else
				{
					$this->ady_covers->remove_files($files);
				}
			}

			if ([] !== $approve)
			{
				$files = $this->fetch_covers($approve);
				$rows = $this->ady_covers->fetch_cover_approved($approve);
				$covers = $this->ady_covers->fetch_and_parse_for_tabnews($rows);
				$updated = $this->ady_covers->update_tab_news($covers);
				$this->ady_releases->update($approve);

				if (!$updated) $error += 2;

				try
				{
					$this->publisher->publish($rows);
				}
				catch (\Throwable $exception)
				{
					$this->log->add(
						'critical',
						$this->user->data['user_id'],
						$this->user->ip,
						'LOG_DCTNEWS_RELEASES_DISCORD_FAILS',
						false,
						[$exception->getMessage()]
					);
				}
			}

			if ($error >= 2)
			{
				$this->template->assign_vars([
					"ERROR" => $error,
					"CODE"  => self::ERROR_NAME,
				]);

				return $this->helper->render('error.html', self::ERROR_PAGE_TITLE);
			}

			if ([] !== $approve)
			{
				$delete = $this->ady_covers->delete_request($approve);

				if (!$delete) $error += 4;
			}

			$this->template->assign_vars([
				"SUCCESS" => "approved",
				"ERROR"   => $error,
				"CODE"    => self::ERROR_NAME,
			]);

			return $this->helper->render('success.html', self::SUCCESS_PAGE_TITLE);
		}

		$sectionsTitles = [
			sections::DC     => "DC Rebirth",
			sections::HORSDC => "DC Hors Rebirth",
			sections::INDE   => "Indépendant",
			sections::MARVEL => "Marvel",
		];

		$this->template->assign_vars([
			"ALL_REQUEST"     => $this->ady_covers->format_covers_to_approve($to_approve),
			"DISPLAY_SECTION" => $sectionsTitles,
		]);

		return $this->helper->render('approve.html', self::ADMIN_PAGE_TITLE);
	}

	private function fetch_covers($ids): array
	{
		$files = [];
		$covers = $this->ady_covers->fetch_cover_approved($ids);
		foreach ($covers as $cover)
		{
			$files[] = $cover['path_cover'];
		}

		return $files;
	}

	private function process_v1(string $cover): publication
	{
		$header = $this->request->variable('header', ['' => ''], true);
		$month = $this->request->variable('month', ['' => ''], true);
		$status = $this->request->variable('status', 0, true);
		$index = $this->request->variable('index', false, true);

		$publication = new publication();
		$publication->editor = $header['section'];
		$publication->url_release = $header['url'];
		$publication->path_cover = $cover;
		$publication->user_id = $this->user->data['user_id'];
		$publication->serie_title = $month['title'];
		$publication->issue_title = null;
		$publication->issue_number = $month['number'];
		$publication->comics_format = null;
		$publication->comics_index = $index;
		$publication->status = $status;
		
		return $publication;
	}

	private function process_v2(string $cover): publication
	{
		$request = new pub_request();
		$request->is_first_issue = $this->request->variable('publication-type', false, true);
		$request->publish_post = $this->request->variable('publication-action', false, true);
		$request->description = $this->request->variable('message', '', true);
		$request->editor = $this->request->variable('editor', 0, true);

		$serie = $this->request->variable('serie', ['' => ''], true);
		$request->serie->id = filter_var($serie['id'], FILTER_VALIDATE_INT) ?? null;
		$request->serie->title = '' !== $serie['title'] ? $serie['title'] : null;
		$request->serie->forum_id = filter_var($serie['forum'], FILTER_VALIDATE_INT) ?? null;
		$request->serie->share_link = '' !== $serie['share'] ? $serie['share'] : null;
		$request->serie->status = filter_var($serie['status'], FILTER_VALIDATE_INT) ?? null;
		$request->serie->url = '' !== $serie['url'] ? $serie['url'] : null;

		$issue = $this->request->variable('issue', ['' => ''], true);
		$request->issue->cover = $cover;
		$request->issue->number = '' !== $issue['number'] ? $issue['number'] : null;
		$request->issue->title = '' !== $issue['title'] ? $issue['title'] : null;
		$request->issue->format = filter_var($issue['format'], FILTER_VALIDATE_INT) ?? null;

		$team = $this->request->variable('team', ['' => ''], true);
		foreach ($team as $post => $ids)
		{
			if ("" === $ids) continue;
			$request->team->$post = (array) json_decode(html_entity_decode($ids));
		}

		if ($request->is_first_issue && !$request->publish_post)
		{
			return $this->transformRequestToPublication($request);
		}

		if ($request->is_first_issue)
		{
			$data['forum_id'] = $request->serie->forum_id;
			$data['topic_title'] = $request->serie->title;
			$data['message'] = $this->ady_publish->create_new_post($request);

			$request->serie->url = $this->ady_publish->post_comic($data, $request->is_first_issue);
	
			return $this->transformRequestToPublication($request);
		}

		$index_entry = $this->ady_indexcomics->fetch_one($request->serie->id);
		if (null === $index_entry)
		{
			throw new \Exception('DCTNEWS_REQUEST_ERROR_SERIES');
		}
		$request->serie->title = $index_entry['title'];

		if (!$request->publish_post)
		{
			return $this->transformRequestToPublication($request);
		}

		$request->serie->url = $index_entry['url'];
		parse_str(parse_url(htmlspecialchars_decode($index_entry['url']))['query'], $result);
		$data['topic_id'] = $result['t'];
		$data['message'] = $this->ady_publish->create_reply_post($request);

		$request->serie->url = $this->ady_publish->post_comic($data, $request->is_first_issue);

		return $this->transformRequestToPublication($request);
	}

	private function transformRequestToPublication(pub_request $request): publication
	{

		$publication = new publication();
		$publication->editor = $request->editor;
		$publication->url_release = $request->serie->url;
		$publication->path_cover = $request->issue->cover;
		$publication->user_id = $this->user->data['user_id'];
		$publication->serie_title = $request->serie->title;
		$publication->issue_title = $request->issue->title;
		$publication->issue_number = $request->issue->number;
		$publication->comics_format = $request->issue->format;
		$publication->comics_index = $request->is_first_issue;
		$publication->status = $request->serie->status;

		return $publication;
	}
}
