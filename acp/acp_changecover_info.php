<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\acp;

/**
 * @package module_install
 */
class acp_changecover_info
{
	function module()
	{
		return [
			'filename' => '\ady\changecover\acp\acp_changecover_module',
			'title'    => 'TN_ACP',
			'version'  => '2.0.0',
			'modes'    => [
				'settings' => [
					'title' => 'TN_CONFIG',
					'auth'  => 'ext_ady/changecover',
					'cat'   => ['TABNEWS_TABNEWS_MOD'],
				],
			],
		];
	}
}
