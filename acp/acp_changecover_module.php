<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\acp;

class acp_changecover_module
{
	/**
	 * @var string
	 */
	public $u_action;
	/**
	 * @var string
	 */
	public $tpl_name;
	/**
	 * @var mixed
	 */
	public $page_title;

	function main($id, $mode)
	{
		global $user, $template, $request, $phpbb_container;
		global $config;

		$config_text = $phpbb_container->get('config_text');

		$user->add_lang('acp/common');
		$user->add_lang_ext('ady/changecover', 'acp/info_acp_tabnews');
		$this->tpl_name = 'acp_tabnews';
		$this->page_title = $user->lang['TABNEWS_TABNEWS_MOD'];
		add_form_key('acp_tabnews');

		if ($request->is_set_post('submit'))
		{
			if (!check_form_key('acp_tabnews'))
			{
				meta_refresh(3, $this->u_action);
				trigger_error('FORM_INVALID');
			}

			$config->set('tn_status', $request->variable('tn_status', true));
			$config->set('tn_type', $request->variable('tn_type', true));
			$config->set('tn_firstpost', $request->variable('tn_firstpost', true));
			$config->set('tn_firstpost4', $request->variable('tn_firstpost4', true));
			$config->set('active_tab3', $request->variable('active_tab3', true));
			$config->set('tn_forum7', $request->variable('tn_forum7', true));
			$config->set('tn_facebookpage', $request->variable('tn_facebookpage', true));
			$config->set('tabnews_tn_code', $request->variable('tn_code', '', true));
			$config->set('title_tab1', $request->variable('title_tab1', '', true));
			$config->set('title_tab3', $request->variable('title_tab3', '', true));
			$config->set('tabnews_tn_code_tab', $request->variable('tn_code_tab', '', true));
			$config->set('tabnews_tn_code_tab4', $request->variable('tn_code_tab4', '', true));
			$config->set('active_tab5', $request->variable('active_tab5', true));
			$config->set('title_tab5', $request->variable('title_tab5', '', true));
			$config->set('active_tab6', $request->variable('active_tab6', true));
			$config->set('title_tab6', $request->variable('title_tab6', '', true));
			$config_text->set('tabnews_urlpage', $request->variable('tabnews_urlpage', '', true));
			$config_text->set('tabnews2_urlpage', $request->variable('tabnews2_urlpage', '', true));
			$config_text->set('tabnews4_urlpage', $request->variable('tabnews4_urlpage', '', true));
			$config_text->set('tabnews_text', $request->variable('tabnews_text', '', true));
			$config_text->set('tabnews5_urlpage', $request->variable('tabnews5_urlpage', '', true));
			$config_text->set('tabnews6_urlpage', $request->variable('tabnews6_urlpage', '', true));
			$config_text->set('dctnews_webhook_url', $request->variable('webhook_url', '', true));

			meta_refresh(3, $this->u_action);
			trigger_error($user->lang['TN_SAVED'] . adm_back_link($this->u_action));
		}

		$tabnews_data = $config_text->get_array(['tabnews_text']);
		$tabnews2_data = $config_text->get_array(['tabnews_urlpage']);
		$tabnews3_data = $config_text->get_array(['tabnews2_urlpage']);
		$tabnews4_data = $config_text->get_array(['tabnews4_urlpage']);
		$tabnews5_data = $config_text->get_array(['tabnews5_urlpage']);
		$tabnews6_data = $config_text->get_array(['tabnews6_urlpage']);
		$webhook_url = $config_text->get('dctnews_webhook_url');

		$template->assign_vars([
			'STATUS'           => $config['tn_status'],
			'TYPE'             => $config['tn_type'],
			'FIRSTPOST'        => $config['tn_firstpost'],
			'FIRSTPOST4'       => $config['tn_firstpost4'],
			'ACTIVE_TAB3'      => $config['active_tab3'],
			'FORUM7'           => $config['tn_forum7'],
			'FACEBOOKPAGE'     => $config['tn_facebookpage'],
			'CODE'             => $config['tabnews_tn_code'],
			'URLPAGE'          => $config['tabnews_tn_urlpage'],
			'TITLE_TAB1'       => $config['title_tab1'],
			'TITLE_TAB3'       => $config['title_tab3'],
			'CODE_TAB'         => $config['tabnews_tn_code_tab'],
			'CODE_TAB4'        => $config['tabnews_tn_code_tab4'],
			'ACTIVE_TAB5'      => $config['active_tab5'],
			'TITLE_TAB5'       => $config['title_tab5'],
			'ACTIVE_TAB6'      => $config['active_tab6'],
			'TITLE_TAB6'       => $config['title_tab6'],
			'TABNEWS_URLPAGE'  => $tabnews2_data['tabnews_urlpage'],
			'TABNEWS2_URLPAGE' => $tabnews3_data['tabnews2_urlpage'],
			'TABNEWS4_URLPAGE' => $tabnews4_data['tabnews4_urlpage'],
			'TABNEWS_TEXT'     => $tabnews_data['tabnews_text'],
			'TABNEWS5_URLPAGE' => $tabnews5_data['tabnews5_urlpage'],
			'TABNEWS6_URLPAGE' => $tabnews6_data['tabnews6_urlpage'],
			'WEBHOOK_URL'      => $webhook_url,
			'U_ACTION'         => $this->u_action,
		]);
	}
}
