<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\migrations;

use ady\changecover\constant\tables;

class changecover_1_2_1 extends \phpbb\db\migration\migration
{
	public function update_schema()
	{
		return [
			'add_columns'    => [
				$this->table_prefix . tables::MONTHLY_RELEASE => [
					'section' => ['UINT', 0],
					'cover'   => ['VCHAR:65', ''],
				],
			],
			'change_columns' => [
				$this->table_prefix . tables::COVER_TOAPPROVE => [
					'section' => ['UINT', 0],
				],
			],
		];
	}

	public function revert_schema()
	{
		return [
			'drop_columns'   => [
				$this->table_prefix . tables::MONTHLY_RELEASE => [
					'section',
					'cover',
				],
			],
			'change_columns' => [
				$this->table_prefix . tables::COVER_TOAPPROVE => [
					'section' => ['VCHAR:15', ''],
				],
			],
		];
	}
}
