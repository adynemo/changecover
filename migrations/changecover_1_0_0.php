<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\migrations;

use ady\changecover\constant\tables;

class changecover_1_0_0 extends \phpbb\db\migration\migration
{
	public function update_schema()
	{
		return [
			'add_tables' => [
				$this->table_prefix . tables::COVER_TOAPPROVE => [
					'COLUMNS'     => [
						'id'          => ['UINT', null, 'auto_increment'],
						'section'     => ['VCHAR:15', ''],
						'url_release' => ['VCHAR:255', ''],
						'path_cover'  => ['VCHAR:65', ''],
						'user_id'     => ['UINT', 0],
					],
					'PRIMARY_KEY' => 'id',
				],
			],
		];
	}

	public function revert_schema()
	{
		return [
			'drop_tables' => [
				$this->table_prefix . tables::COVER_TOAPPROVE,
			],
		];
	}

	public function update_data()
	{
		return [
			['permission.add', ['u_changecover_requester', true]],
			['permission.add', ['u_changecover_approver', true]],
		];
	}
}
