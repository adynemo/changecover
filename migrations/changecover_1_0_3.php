<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\migrations;

class changecover_1_0_3 extends \phpbb\db\migration\migration
{
	public function update_data()
	{
		return [
			['permission.add', ['a_changecover_api', true]],
		];
	}
}
