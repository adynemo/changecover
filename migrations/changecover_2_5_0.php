<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2023 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\migrations;

use ady\changecover\constant\tables;
use phpbb\db\migration\migration;

class changecover_2_5_0 extends migration
{
	public function update_schema(): array
	{
		return [
			'add_columns'  => [
				$this->table_prefix . tables::COVER_TOAPPROVE => [
					'serie_title'   => ['VCHAR:255', ''],
					'issue_title'   => ['VCHAR:255', null],
					'issue_number'  => ['VCHAR:255', ''],
					'comics_format' => ['UINT', null],
					'editor'        => ['UINT', 0],
				],
				$this->table_prefix . tables::COMICS_INDEX    => [
					'editor' => ['UINT', null],
				],
			],
			'drop_columns' => [
				$this->table_prefix . tables::COVER_TOAPPROVE => [
					'title',
					'number',
					'section',
				],
			],
		];
	}

	public function revert_schema(): array
	{
		return [
			'add_columns'  => [
				$this->table_prefix . tables::COVER_TOAPPROVE => [
					'title'  => ['VCHAR:255', ''],
					'number' => ['VCHAR:255', ''],
				],
			],
			'drop_columns' => [
				$this->table_prefix . tables::COVER_TOAPPROVE => [
					'serie_title',
					'issue_title',
					'issue_number',
					'comics_format',
				],
				$this->table_prefix . tables::COMICS_INDEX    => [
					'editor',
				],
			],
		];
	}
}
