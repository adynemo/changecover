<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\migrations;

use ady\changecover\constant\tables;

class changecover_1_0_2 extends \phpbb\db\migration\migration
{
	public function update_schema()
	{
		return [
			'add_columns'  => [
				$this->table_prefix . tables::COVER_TOAPPROVE => [
					'number'       => ['VCHAR:255', ''],
					'comics_index' => ['BOOL', 0],
				],
			],
			'drop_columns' => [
				$this->table_prefix . tables::MONTHLY_RELEASE => [
					'user_id',
				],
			],
			'add_tables'   => [
				$this->table_prefix . tables::COMICS_INDEX => [
					'COLUMNS'     => [
						'id'    => ['UINT', null, 'auto_increment'],
						'sort'  => ['VCHAR:1', ''],
						'title' => ['VCHAR:255', ''],
						'url'   => ['VCHAR:255', ''],
					],
					'PRIMARY_KEY' => 'id',
				],
			],
		];
	}

	public function revert_schema()
	{
		return [
			'drop_columns' => [
				$this->table_prefix . tables::COVER_TOAPPROVE => [
					'number',
					'comics_index',
				],
			],
			'add_columns'  => [
				$this->table_prefix . tables::MONTHLY_RELEASE => [
					'user_id' => ['UINT', 0],
				],
			],
			'drop_tables'  => [
				$this->table_prefix . tables::COMICS_INDEX,
			],
		];
	}
}
