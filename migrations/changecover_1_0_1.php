<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\migrations;

use ady\changecover\constant\tables;

class changecover_1_0_1 extends \phpbb\db\migration\migration
{
	public function update_schema()
	{
		return [
			'add_columns' => [
				$this->table_prefix . tables::COVER_TOAPPROVE => [
					'title' => ['VCHAR:255', ''],
				],
			],
			'add_tables'  => [
				$this->table_prefix . tables::MONTHLY_RELEASE => [
					'COLUMNS'     => [
						'id'      => ['UINT', null, 'auto_increment'],
						'date'    => ['VCHAR:10', ''],
						'title'   => ['VCHAR:255', ''],
						'url'     => ['VCHAR:255', ''],
						'user_id' => ['UINT', 0],
					],
					'PRIMARY_KEY' => 'id',
				],
			],
		];
	}

	public function revert_schema()
	{
		return [
			'drop_columns' => [
				$this->table_prefix . tables::COVER_TOAPPROVE => [
					'title',
				],
			],
			'drop_tables'  => [
				$this->table_prefix . tables::MONTHLY_RELEASE,
			],
		];
	}
}
