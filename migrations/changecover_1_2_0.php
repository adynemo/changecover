<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\migrations;

use ady\changecover\constant\tables;

class changecover_1_2_0 extends \phpbb\db\migration\migration
{
	public function update_schema()
	{
		return [
			'add_tables' => [
				$this->table_prefix . tables::NEWS => [
					'COLUMNS'     => [
						'id'          => ['UINT', null, 'auto_increment'],
						'title'       => ['VCHAR:20', ''],
						'description' => ['TEXT', ''],
						'url'         => ['VCHAR:255', ''],
						'image'       => ['VCHAR:65', ''],
						'comment'     => ['TEXT', ''],
						'user_id'     => ['UINT', 0],
						'created_at'  => ['UINT:11', 0],
						'posted_at'   => ['UINT:11', 0],
						'deleted_at'  => ['UINT:11', 0],
					],
					'PRIMARY_KEY' => 'id',
				],
			],
		];
	}

	public function revert_schema()
	{
		return [
			'drop_tables' => [
				$this->table_prefix . tables::NEWS,
			],
		];
	}

	public function update_data()
	{
		return [
			['permission.add', ['u_changecover_tabnews_requester', true]],
			['permission.add', ['a_changecover_tabnews_approver', true]],
		];
	}
}
