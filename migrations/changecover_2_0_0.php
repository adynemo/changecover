<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\migrations;

class changecover_2_0_0 extends \phpbb\db\migration\migration
{
	public function update_data()
	{
		return [
			['config.add', ['tn_status', 1]],
			['config.add', ['tn_type', 1]],
			['config.add', ['tn_facebook', 1]],
			['config.add', ['tn_twitter', 1]],
			['config.add', ['tn_tuenti', 1]],
			['config.add', ['tn_sonico', 1]],
			['config.add', ['tn_friendfeed', 1]],
			['config.add', ['tn_digg', 1]],
			['config.add', ['tn_delicious', 1]],
			['config.add', ['tn_vk', 1]],
			['config.add', ['tn_tumblr', 1]],
			['config.add', ['tn_google', 1]],
			['config.add', ['tn_reddit', 1]],
			['config.add', ['tn_all', 0]],
			['config.add', ['tabnews_tn_code', '']],
			['config.add', ['tabnews_tn_urlpage', '']],
			['config.add', ['tabnews_tn_code_tab', '']],
			['config.add', ['tn_facebookpage', 0]],
			['config.add', ['tn_forum7', 0]],
			['config.add', ['tn_firstpost', 0]],
			['config_text.add', ['tabnews_text', '']],
			['config_text.add', ['tabnews_urlpage', '']],

			['module.add', [
				'acp',
				'ACP_CAT_DOT_MODS',
				'TN_ACP',
			]],
			['module.add', [
				'acp',
				'TN_ACP',
				[
					'module_basename' => '\ady\changecover\acp\acp_changecover_module',
					'modes'           => ['settings'],
				],
			]],
		];
	}
}
