<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\migrations;

use ady\changecover\constant\tables;

class changecover_1_1_1 extends \phpbb\db\migration\migration
{
	public function update_schema()
	{
		return [
			'add_columns' => [
				$this->table_prefix . tables::COMICS_INDEX => [
					'hide' => ['BOOL', 0],
				],
			],
		];
	}

	public function revert_schema()
	{
		return [
			'drop_columns' => [
				$this->table_prefix . tables::COMICS_INDEX => [
					'hide',
				],
			],
		];
	}

	public function update_data()
	{
		return [
			['permission.add', ['u_changecover_view_hide_index', true]],
			['permission.add', ['a_changecover_edit_index', true]],
		];
	}
}
