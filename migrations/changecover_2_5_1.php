<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2023 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\migrations;

use ady\changecover\constant\tables;
use phpbb\db\migration\migration;

class changecover_2_5_1 extends migration
{
	public function update_schema(): array
	{
		return [
			'add_columns'  => [
				$this->table_prefix . tables::COMICS_INDEX => [
					'sorted_title' => ['VCHAR:255', ''],
				],
			],
		];
	}

	public function revert_schema(): array
	{
		return [
			'drop_columns' => [
				$this->table_prefix . tables::COMICS_INDEX => [
					'sorted_title',
				],
			],
		];
	}

	public function update_data()
	{
		return [
			['custom', [
				[$this, 'insert_sorted_title']
			]],
		];
	}

	public function insert_sorted_title($value)
	{
		$table = $this->table_prefix . tables::COMICS_INDEX;
		$result = $this->sql_query("SELECT COUNT(id) AS count FROM $table");
		if (!$result)
		{
			throw new \Exception('Unable to complete the migration 2.5.1');
		}

		$row = $this->db->sql_fetchrowset($result)[0];
		$this->db->sql_freeresult($result);
		$count = (int) $row['count'];

		$sql = <<<EOT
UPDATE $table t1
INNER JOIN $table t2 on t1.id = t2.id 
SET t1.sorted_title = t2.title;
EOT;

		$result = $this->sql_query($sql);
		if (!($result && $count === $this->db->sql_affectedrows()))
		{
			throw new \Exception('Something went wrong with the migration 2.5.1');
		}
	}
}
