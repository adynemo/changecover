<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\core;

use ady\changecover\constant\tables;
use phpbb\db\driver\driver_interface;

class releases
{
	private driver_interface $db;
	private string $table_prefix;
	private publish $publish;
	private string $table_changecover_toapprove;
	private string $table_monthly_release;

	public function __construct(
		driver_interface $db,
		publish $publish,
		string $table_prefix
	)
	{
		$this->db = $db;
		$this->publish = $publish;
		$this->table_prefix = $table_prefix;

		$this->table_changecover_toapprove = $this->table_prefix . tables::COVER_TOAPPROVE;
		$this->table_monthly_release = $this->table_prefix . tables::MONTHLY_RELEASE;
	}

	public function update(array $ids): bool
	{
		$table = $this->table_changecover_toapprove;
		$where = $this->db->sql_in_set('id', $ids);
		$sql = <<<EOT
SELECT editor, url_release, path_cover, serie_title, issue_title, issue_number, comics_format
FROM $table
WHERE $where
EOT;

		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return false;
		}
		$rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		$releases = [];
		foreach ($rows as $row)
		{
			$releases[] = [
				'date'    => date('Y-m-d'),
				'title'   => $this->publish->build_title(
					$row['serie_title'],
					$row['issue_title'],
					$row['issue_number'],
					$row['comics_format']
				),
				'url'     => $row['url_release'],
				'cover'   => $row['path_cover'],
				'section' => $row['editor'],
			];
		}

		return $this->db->sql_multi_insert($this->table_monthly_release, $releases);
	}

	public function fetch_by_date($year, $month): array
	{
		$releases = ['count' => 0];
		$month = \DateTime::createFromFormat('m', $month);
		$month = $month->format('m');
		$condition = $year . '-' . $month . '-%';

		$sql = "SELECT date, title, url, section, cover FROM " . $this->table_monthly_release . " WHERE date LIKE '$condition' ORDER BY date DESC";
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return $releases;
		}
		$rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		$releases['count'] = count($rows);

		foreach ($rows as $row)
		{
			$date = (new \DateTime($row['date']))->format('d/m/Y');
			$releases[$date][] = [
				'title'   => $row['title'],
				'url'     => $row['url'],
				'section' => $row['section'],
				'cover'   => $row['cover'],
			];
		}

		return $releases;
	}

	public function fetch_all(): array
	{
		$releases = [];
		$sql = 'SELECT DATE_FORMAT(date, "%Y-%m") d FROM ' . $this->table_monthly_release . ' GROUP BY d';
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return $releases;
		}
		$rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		foreach ($rows as $row)
		{
			$year = (new \DateTime($row['d']))->format('Y');
			$releases[$year][] = $row['d'];
			arsort($releases[$year]);
		}

		arsort($releases);

		return $releases;
	}

	public function fetch_by_interval(\DateTime $begin_date, \DateTime $end_date): array
	{
		$releases = [];
		$sql = sprintf(
			'SELECT date, title, url FROM %s  WHERE date BETWEEN "%s" AND "%s"',
			$this->table_monthly_release,
			$begin_date->format('Y-m-d'),
			$end_date->format('Y-m-d')
		);
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return $releases;
		}

		while ($row = $this->db->sql_fetchrow($result))
		{
			$releases[] = $row;
		}
		$this->db->sql_freeresult($result);

		asort($releases);

		return array_values($releases);
	}
}
