<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2023 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\core;

use phpbb\db\driver\driver_interface;
use phpbb\user;

class users
{
	private driver_interface $db;
	private user $user;
	private core $ady_core;

	public function __construct(driver_interface $db, user $user, core $ady_core)
	{
		$this->db = $db;
		$this->user = $user;
		$this->ady_core = $ady_core;
	}

	public function find_users_by_ids(array $user_ids): array
	{
		$users = [];
		if ([] === $user_ids)
		{
			return $users;
		}

		$user_ids = implode(',', $user_ids);

		$sql = 'SELECT * FROM ' . USERS_TABLE . ' WHERE user_id IN (' . $user_ids . ')';
		$result = $this->db->sql_query($sql);

		if (!$result)
		{
			$this->db->sql_freeresult($result);
			return $users;
		}

		while ($row = $this->db->sql_fetchrow($result))
		{
			$row['avatar'] = $this->avatar_img_resize($row);
			$users[$row['user_id']] = $row;
		}
		$this->db->sql_freeresult($result);

		return $users;
	}

	public function find_users_by_query(string $query): array
	{
		$users = [];
		$user_table = USERS_TABLE;
		$user_group_table = USER_GROUP_TABLE;
		$sql = <<<EOT
SELECT * FROM $user_table AS u
INNER JOIN $user_group_table AS ug ON ug.user_id = u.user_id
WHERE u.username_clean LIKE "%$query%"
AND ug.group_id = 8
ORDER BY u.username ASC
EOT;

		$result = $this->db->sql_query($sql);

		if (!$result)
		{
			$this->db->sql_freeresult($result);
			return $users;
		}

		while ($row = $this->db->sql_fetchrow($result))
		{
			$row['avatar'] = $this->avatar_img_resize($row);
			$users[$row['user_id']] = $row;
		}
		$this->db->sql_freeresult($result);

		return $users;
	}

	private function avatar_img_resize(array $row): string
	{
		if (!empty($row['user_avatar']))
		{
			if ($row['user_avatar_width'] >= $row['user_avatar_height'])
			{
				$ratio = $row['user_avatar_width'] / $row['user_avatar_height'];
				$row['user_avatar_height'] = round(30 / $ratio);
				$row['user_avatar_width'] = 30;
			}
			else
			{
				$ratio = $row['user_avatar_height'] / $row['user_avatar_width'];
				$row['user_avatar_width'] = round(30 / $ratio);
				$row['user_avatar_height'] = 30;
			}
			return phpbb_get_user_avatar($row, '');
		}

		$web_path = $this->ady_core->get_board_url();
		$theme = "{$web_path}styles/" . rawurlencode($this->user->style['style_path']) . '/theme';

		return '<img class="avatar" src="' . $theme . '/images/no_avatar.gif" width="' . 30 . '" height="' . 30 . '" alt="">';
	}
}
