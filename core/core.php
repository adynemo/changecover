<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\core;

use ady\changecover\constant\paths;
use ady\changecover\constant\tables;
use ady\changecover\exception\uploadException;
use phpbb\auth\auth;
use phpbb\controller\helper;
use phpbb\db\driver\driver_interface;
use phpbb\exception\http_exception;
use phpbb\log\log;
use phpbb\path_helper;
use phpbb\user;

class core
{
	public const WIDTH = "W";
	public const HEIGHT = "H";

	private auth $auth;
	private driver_interface $db;
	private path_helper $path_helper;
	private log $log;
	private user $user;
	private helper $helper;
	private string $table_prefix;
	private string $root_path;
	private string $table_users;

	public function __construct(
		auth $auth,
		driver_interface $db,
		path_helper $path_helper,
		log $log,
		user $user,
		helper $helper,
		$table_prefix,
		$root_path
	)
	{
		$this->auth = $auth;
		$this->db = $db;
		$this->path_helper = $path_helper;
		$this->table_prefix = $table_prefix;
		$this->root_path = $root_path;
		$this->log = $log;
		$this->user = $user;
		$this->helper = $helper;

		$this->table_users = $this->table_prefix . tables::USERS;
	}

	public function checkAuth($permission, $user)
	{
		if (!$this->auth->acl_get($permission))
		{
			if (!$user->data['is_registered'])
			{
				login_box();
			}

			throw new http_exception(403, 'NOT_AUTHORISED');
		}
	}

	public function fetch_username($user_id): string
	{
		$sql = "SELECT username FROM " . $this->table_users . " WHERE " . $this->db->sql_build_array('SELECT', ['user_id' => $user_id]);
		$result = $this->db->sql_query($sql);
		$row = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		return $row[0]['username'];
	}

	public function save_image($file, $image): string
	{
		$name = md5(uniqid(rand(), true));
		$ext_upload = strtolower(substr(strrchr($file['name'], '.'), 1));
		$filename = sprintf('%s.%s', $name, $ext_upload);
		$name_ext = paths::COVERS_PATH . '/' . $filename;
		$result = imagejpeg($image, $name_ext, 70);
		imagedestroy($image);
		if (!$result)
		{
			throw new uploadException('DCTNEWS_UPLOAD_ERROR_FAILURE');
		}

		return $filename;
	}

	public function check_image($file): bool
	{
		if ($file['error'] == UPLOAD_ERR_NO_FILE)
		{
			throw new uploadException('DCTNEWS_UPLOAD_ERROR_NO_FILE');
		}
		
		if ($file['error'] != UPLOAD_ERR_OK)
		{
			throw new uploadException('DCTNEWS_UPLOAD_ERROR_FILE_ERROR', $file['error']);
		}
		
		if ($file['size'] > 20971520)
		{
			throw new uploadException('DCTNEWS_UPLOAD_ERROR_TOO_BIG_FILE');
		}
		
		$ext_upload = strtolower(substr(strrchr($file['name'], '.'), 1));
		if (!in_array($ext_upload, ['jpg', 'jpeg', 'png']))
		{
			throw new uploadException('DCTNEWS_UPLOAD_ERROR_EXT_ERROR');
		}

		return true;
	}

	/**
	 * http://memo-web.fr/categorie-php-197.php
	 */
	public function resize_image(string $source, string $type_value, int $new_value)
	{
		if ([] === ([$source_largeur, $source_hauteur] = @getimagesize($source)))
		{
			return false;
		}

		if ($type_value === self::HEIGHT)
		{
			$nouv_hauteur = $new_value;
			$nouv_largeur = ($new_value / $source_hauteur) * $source_largeur;
		}
		else
		{
			$nouv_largeur = $new_value;
			$nouv_hauteur = ($new_value / $source_largeur) * $source_hauteur;
		}

		// Création du conteneur.
		$image = imagecreatetruecolor($nouv_largeur, $nouv_hauteur);

		// Importation de l'image source.
		$source_image = imagecreatefromstring(file_get_contents($source));

		// Copie de l'image dans le nouveau conteneur en la rééchantillonant.
		imagecopyresampled($image, $source_image, 0, 0, 0, 0, $nouv_largeur, $nouv_hauteur, $source_largeur, $source_hauteur);

		// Libération de la mémoire allouée aux deux images (sources et nouvelle).
		imagedestroy($source_image);

		return $image;
	}

	public function remove_file(string $filename): void
	{
		unlink($this->root_path . $filename);
	}

	public function get_board_url(): string
	{
		$board_url = generate_board_url() . '/';
		$corrected_path = $this->path_helper->get_web_root_path();

		return (defined('PHPBB_USE_BOARD_URL_PATH') && PHPBB_USE_BOARD_URL_PATH) ? $board_url : $corrected_path;
	}

	public function log_visit(): void
	{
		try {
			$this->log->add(
				'user',
				$this->user->data['user_id'],
				$this->user->ip,
				'Visit: ' . $this->helper->get_current_url(),
				false,
				['reportee_id' => $this->user->data['user_id']]
			);
		} catch (\Throwable $exception) {
			// explicitly ignored
		}
	}
}
