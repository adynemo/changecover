<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\core;

use ady\changecover\constant\tables;
use ady\changecover\entity\index;
use phpbb\db\driver\driver_interface;

class indexComics
{
	private driver_interface $db;
	private string $table_prefix;
	private string $table_comics_index;
	private string $table_changecover_toapprove;

	private const ARTICLES = ['the', 'a', 'an', 'le', 'la', 'un', 'une', 'les'];

	public function __construct(
		driver_interface $db,
		string $table_prefix
	)
	{
		$this->db = $db;
		$this->table_prefix = $table_prefix;

		$this->table_comics_index = $this->table_prefix . tables::COMICS_INDEX;
		$this->table_changecover_toapprove = $this->table_prefix . tables::COVER_TOAPPROVE;
	}

	public function fetch_by_character(string $character): ?array
	{
		return $this->fetch_by(['sort' => $character]);
	}

	public function fetch_by_status(int $status): ?array
	{
		return $this->fetch_by(['status' => $status]);
	}

	public function fetch_one(int $id): ?array
	{
		$selectStmt = $this->db->sql_build_array('SELECT', ['id' => $id]);
		$columns = $this->get_columns_stmt();
		$sql = <<<EOT
SELECT $columns
FROM $this->table_comics_index
WHERE $selectStmt
EOT;
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return null;
		}
		$row = $this->db->sql_fetchrowset($result)[0];
		$this->db->sql_freeresult($result);

		return $row;
	}

	public function update(int $id, array $element): bool
	{
		$updateStmt = $this->db->sql_build_array('UPDATE', $this->sort_entry($element)->toDatabase());
		$sql = <<<EOT
UPDATE $this->table_comics_index
SET $updateStmt
WHERE id = '$id'
EOT;

		return !!$this->db->sql_query($sql);
	}

	public function delete(int $id): bool
	{
		$sql = "DELETE FROM " . $this->table_comics_index . " WHERE " . $this->db->sql_build_array('DELETE', ['id' => $id]);

		return !!$this->db->sql_query($sql);
	}

	public function insert(array $ids): bool
	{
		$sql = sprintf(
			'SELECT editor, url_release AS url, serie_title AS title, status FROM %s WHERE %s',
			$this->table_changecover_toapprove,
			$this->db->sql_in_set('id', $ids)
		);
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return false;
		}
		$rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		foreach ($rows as $row)
		{
			if (!(empty($row['title']) || $this->create($row)))
			{
				return false;
			}
		}

		return true;
	}

	public function create(array $element): bool
	{
		$insertStmt = $this->db->sql_build_array('INSERT', $this->sort_entry($element)->toDatabase());
		$sql = <<<EOT
INSERT INTO $this->table_comics_index
$insertStmt
EOT;

		return !!$this->db->sql_query($sql);
	}

	private function sort_entry(array $row): index
	{
		$entry = (new index())
			->setTitle($row['title'])
			->setSortedTitle($row['title'])
			->setSortChar('#')
			->setUrl($row['url'])
			->setHidden(!empty($row['hide']))
			->setStatus($row['status'])
			->setEditor($row['editor']);

		$articles = implode('|', self::ARTICLES);
		$pattern = "/^(($articles)\s+|l')(.+)/i";
		preg_match($pattern, $entry->getTitle(), $result);
		if (!empty($result))
		{
			$entry->setSortedTitle($result[3] . ', ' . rtrim($result[1]));
		}

		$alph = range('a', 'z');
		$first_letter = strtolower($entry->getSortedTitle()[0] ?? '');
		$sortChar = in_array($first_letter, $alph) ? $first_letter : $entry->getSortChar();
		$entry->setSortChar($sortChar);

		return $entry;
	}

	public function find_entry_by_query(string $query): array
	{
		$entries = [];
		$table = $this->table_comics_index;
		$columns = $this->get_columns_stmt();
		$sql = <<<EOT
SELECT $columns
FROM $table
WHERE LOWER(title) LIKE LOWER("%$query%")
ORDER BY sorted_title ASC
LIMIT 0, 20
EOT;

		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			$this->db->sql_freeresult($result);

			return $entries;
		}

		while ($row = $this->db->sql_fetchrow($result))
		{
			$entries[$row['id']] = $row;
		}
		$this->db->sql_freeresult($result);

		return $entries;
	}

	private function fetch_by(array $condition): ?array
	{
		$selectStmt = $this->db->sql_build_array('SELECT', $condition);
		$columns = $this->get_columns_stmt();
		$sql = <<<EOT
SELECT $columns
FROM $this->table_comics_index
WHERE $selectStmt
ORDER BY sorted_title ASC
EOT;
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return null;
		}
		$rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		return $rows;
	}

	private function get_columns_stmt(): string
	{
		return implode(',', tables::COMICS_INDEX_COLUMNS);
	}
}
