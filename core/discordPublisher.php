<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\core;

use ady\changecover\constant\paths;
use ady\changecover\constant\sections;
use ady\changecover\object\discord\Image;
use ady\changecover\object\discord\Release;
use ady\changecover\object\discord\Releases;
use http\Client;
use http\Client\Request;
use http\Message\Body;
use phpbb\config\db_text;
use phpbb\log\log;
use phpbb\user;

class discordPublisher
{
	private const COLORS_MAP = [
		sections::DC     => 210,
		sections::HORSDC => 210,
		sections::INDE   => 2067276,
		sections::MARVEL => 15158332,
	];

	private db_text $config;
	private log $log;
	private user $user;
	private publish $ady_publish;

	public function __construct(
		db_text $config,
		log $log,
		user $user,
		publish $ady_publish
	)
	{
		$this->config = $config;
		$this->log = $log;
		$this->user = $user;
		$this->ady_publish = $ady_publish;
	}

	public function publish(array $data): bool
	{
		$embeds = new Releases();

		foreach ($data as $release)
		{
			$embed = new Release();
			$embed->title = $this->ady_publish->build_title(
				$release['serie_title'],
				$release['issue_title'],
				$release['issue_number'],
				$release['comics_format']
			);
			$embed->url = $release['url_release'];
			$embed->color = self::COLORS_MAP[$release['editor']];

			$image = new Image();
			$image->url = sprintf('%1$s/%2$s/%3$s', generate_board_url(), paths::COVERS_PATH, $release['path_cover']);

			$embed->image = $image;
			$embeds->addEmbed($embed);
		}

		return $this->do_request($embeds);
	}

	private function do_request(Releases $data): bool
	{
		$url = $this->config->get('dctnews_webhook_url');
		if (empty($url))
		{
			return false;
		}

		$this->log->add(
			'admin',
			$this->user->data['user_id'],
			$this->user->ip,
			'LOG_DCTNEWS_RELEASES_DISCORD_SEND'
		);

		$client = new Client();
		$request = new Request();

		$body = new Body();
		$body->append(json_encode($data));

		$request->setRequestUrl($url);
		$request->setRequestMethod('POST');
		$request->setBody($body);

		$request->setHeaders([
			'Content-Type' => 'application/json',
		]);

		$client->enqueue($request)->send();
		$response = $client->getResponse();

		$success = 204 === $response->getResponseCode();
		if (!$success)
		{
			$this->log->add(
				'critical',
				$this->user->data['user_id'],
				$this->user->ip,
				'LOG_DCTNEWS_RELEASES_DISCORD_FAILS',
				false,
				[$response->serialize()]
			);
		}

		return $success;
	}
}
