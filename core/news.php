<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\core;

use ady\changecover\constant\paths;
use ady\changecover\constant\tables;
use ady\changecover\constant\tabnews;
use ady\changecover\exception\uploadException;
use phpbb\db\driver\driver_interface;

class news
{
	private const SIZE = 300;

	private string $table_news;
	private string $table_config_text;
	private driver_interface $db;
	private core $ady_core;
	private covers $ady_covers;
	private string $table_prefix;

	public function __construct(
		driver_interface $db,
		core $ady_core,
		covers $ady_covers,
		string $table_prefix
	)
	{
		$this->db = $db;
		$this->ady_core = $ady_core;
		$this->ady_covers = $ady_covers;
		$this->table_prefix = $table_prefix;

		$this->table_news = $this->table_prefix . tables::NEWS;
		$this->table_config_text = $this->table_prefix . tables::CONFIG_TEXT;
	}

	public function find(): array
	{
		$sql = sprintf('SELECT * FROM %s ORDER BY id DESC', $this->table_news);
		$result = $this->db->sql_query($sql);
		$rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		foreach ($rows as $key => $row)
		{
			$rows[$key]['user'] = $this->ady_core->fetch_username($row['user_id']);

			$rows[$key]['description'] = html_entity_decode(nl2br($row['description']));
			$rows[$key]['comment'] = nl2br($row['comment']);

			$rows[$key]['created_at'] = $this->get_date_from_timestamp($row['created_at']);
			if ($row['posted_at'])
			{
				$rows[$key]['posted_at'] = $this->get_date_from_timestamp($row['posted_at']);
			}
			if ($row['deleted_at'])
			{
				$rows[$key]['deleted_at'] = $this->get_date_from_timestamp($row['deleted_at']);
			}
		}

		return $rows;
	}

	public function find_one(int $id): array
	{
		$sql = sprintf('SELECT * FROM %s WHERE %s', $this->table_news, $this->db->sql_build_array('SELECT', ['id' => $id]));
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return [];
		}
		$row = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		return $row[0] ?? [];
	}

	public function publish(array $news): bool
	{
		// Fetch old news id
		$sql = "SELECT config_value FROM " . $this->table_config_text . " WHERE " . $this->db->sql_build_array('SELECT', ['config_name' => tabnews::TABNEWS_1]);
		$result = $this->db->sql_query($sql);
		$row = $this->db->sql_fetchrow($result);
		$this->db->sql_freeresult($result);
		$old_tabnews = html_entity_decode($row['config_value']);
		$pattern = "/id=\"(\d+)-\w{13}\"/";
		preg_match($pattern, $old_tabnews, $id_search);

		if (isset($id_search[1]) && !empty($id_search[1]))
		{
			// Update old news
			$update_old = [
				'deleted_at' => (new \DateTime('now'))->getTimestamp(),
			];

			$sql = "UPDATE " . $this->table_news . " SET " . $this->db->sql_build_array('UPDATE', $update_old) . " WHERE id = '" . $id_search[1] . "'";

			if (!$this->db->sql_query($sql))
			{
				return false;
			}
		}

		// Replace tabnews value
		$tabnews = sprintf(
			self::NEWS_TEMPLATE,
			 $this->build_news_key($news['id']),
			 $news['title'],
			 $news['url'],
			 '/' . paths::COVERS_PATH . '/' . $news['image'],
			 nl2br(html_entity_decode($news['description']))
		);
		$new_tabnews = [
			"config_value" => htmlentities($tabnews),
		];

		$sql = "UPDATE " . $this->table_config_text . " SET " . $this->db->sql_build_array('UPDATE', $new_tabnews) . " WHERE config_name = '" . tabnews::TABNEWS_1 . "'";

		if (!$this->db->sql_query($sql))
		{
			return false;
		}

		// Update news
		$update_news = [
			'posted_at'  => (new \DateTime('now'))->getTimestamp(),
			'deleted_at' => 0,
		];

		$sql = "UPDATE " . $this->table_news . " SET " . $this->db->sql_build_array('UPDATE', $update_news) . " WHERE id = '" . $news['id'] . "'";
		$this->db->sql_query($sql);

		return !!$this->db->sql_affectedrows();
	}

	public function update(array $new, array $old): bool
	{
		$news = array_merge($old, $new);

		$sql = 'UPDATE ' . $this->table_news . " SET " . $this->db->sql_build_array('UPDATE', $news) . " WHERE id = '" . $news['id'] . "'";
		if (!$this->db->sql_query($sql))
		{
			return false;
		}

		if (isset($new['image']))
		{
			$this->ady_covers->remove_files([$old['image']]);
		}

		return true;
	}

	public function remove(array $news): bool
	{
		$sql = sprintf("DELETE FROM %s WHERE id = %d", $this->table_news, $news['id']);
		$this->db->sql_query($sql);

		$this->ady_covers->remove_files([$news['image']]);

		return !!$this->db->sql_affectedrows();
	}

	public function save(array $news): bool
	{
		$sql = 'INSERT INTO ' . $this->table_news . $this->db->sql_build_array('INSERT', $news);
		$this->db->sql_query($sql);

		return !!$this->db->sql_affectedrows();
	}

	/**
	 * @throws uploadException
	 */
	public function upload_image(array $file): string
	{
		$this->ady_core->check_image($file);

		if ([] === ([$source_largeur, $source_hauteur] = @getimagesize($file['tmp_name'])))
		{
			throw new uploadException('DCTNEWS_UPLOAD_ERROR_SIZE_MISSING');
		}

		$type_value = ($source_largeur > $source_hauteur) ? $this->ady_core::WIDTH : $this->ady_core::HEIGHT;

		$image = $this->ady_core->resize_image($file['tmp_name'], $type_value, self::SIZE);

		return $this->ady_core->save_image($file, $image);
	}

	private function build_news_key($id): string
	{
		return $id . uniqid('-');
	}

	private function get_date_from_timestamp($timestamp): string
	{
		return (new \DateTime())->setTimestamp($timestamp)->format('d-m-Y');
	}

	private const NEWS_TEMPLATE = <<<EOT
<div id="%s" class="news">
    <span>
        <p class="title">%s</p>
        <a href="%s">
            <img src="%s" alt="">
        </a>
        <p class="annotation">Cliquez sur l'image ci-dessus.</p>
        <p class="text">%s</p>
    </span>
</div>
EOT;
}
