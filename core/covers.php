<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */


namespace ady\changecover\core;

use ady\changecover\constant\paths;
use ady\changecover\constant\sections;
use ady\changecover\constant\tables;
use ady\changecover\constant\tabnews;
use ady\changecover\dto\publication\publication;
use ady\changecover\exception\uploadException;
use phpbb\db\driver\driver_interface;

class covers
{
	private const COVERS_HTML = "<a href='%s'><img src='/%s' alt='' title='%s'></a>";
	private const BEGIN_TAG = "<!-- BEGIN COVER -->\n";
	private const END_TAG = "\n<!-- END COVER -->";
	private const WIDTH = 300;

	private core $ady_core;
	private driver_interface $db;
	private publish $publish;
	private string $table_prefix;
	private string $table_changecover_toapprove;
	private string $table_config_text;

	public function __construct(
		core $ady_core,
		driver_interface $db,
		publish $publish,
		string $table_prefix
	)
	{
		$this->ady_core = $ady_core;
		$this->db = $db;
		$this->publish = $publish;
		$this->table_prefix = $table_prefix;

		$this->table_changecover_toapprove = $this->table_prefix . tables::COVER_TOAPPROVE;
		$this->table_config_text = $this->table_prefix . tables::CONFIG_TEXT;
	}

	public function save_request(publication $publication): bool
	{
		$sql = sprintf('INSERT INTO %s %s', $this->table_changecover_toapprove, $this->db->sql_build_array('INSERT', (array)$publication));
		$this->db->sql_query($sql);

		return !!$this->db->sql_affectedrows();
	}

	public function fetch_cover_to_approve(): array
	{
		$sql = "SELECT * FROM " . $this->table_changecover_toapprove;
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return [];
		}
		$rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		return $rows;
	}

	public function format_covers_to_approve(array $covers): array
	{
		$approve = [];
		foreach ($covers as $cover)
		{
			$title = $this->publish->build_title(
				$cover['serie_title'],
				$cover['issue_title'],
				$cover['issue_number'],
				$cover['comics_format']
			);
			$approve[$cover['editor']][] = [
				'ID'           => $cover['id'],
				'COVER'        => sprintf(
					self::COVERS_HTML,
					$cover['url_release'],
					paths::COVERS_PATH . '/' . $cover['path_cover'],
					$title
				),
				'USER'         => $this->ady_core->fetch_username($cover['user_id']),
				'TITLE'        => $title,
				'COMICS_INDEX' => $cover['comics_index'],
			];
		}

		return $approve;
	}

	public function count_cover_to_approve(): int
	{
		$sql = "SELECT COUNT(id) count FROM " . $this->table_changecover_toapprove;
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return 0;
		}
		$row = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		return $row[0]['count'] ?? 0;
	}

	public function fetch_and_parse_for_tabnews(array $results): array
	{
		$covers = [];

		foreach ($results as $cover)
		{
			$html = sprintf(
				self::COVERS_HTML,
				$cover['url_release'],
				paths::COVERS_PATH . '/' . $cover['path_cover'],
				$this->publish->build_title(
					$cover['serie_title'],
					$cover['issue_title'],
					$cover['issue_number'],
					$cover['comics_format']
				)
			);
			$covers[$cover['editor']][] = $html;
		}

		return $covers;
	}

	public function fetch_cover_approved(array $ids): array
	{
		$sql = sprintf(
			'SELECT editor, url_release, path_cover, serie_title, issue_title, issue_number, comics_format FROM %1$s WHERE %2$s',
			$this->table_changecover_toapprove,
			$this->db->sql_in_set('id', $ids)
		);
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return [];
		}
		$rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		return $rows;
	}

	public function update_tab_news(array $covers): bool
	{
		$tabnews_order = [
			sections::DC     => tabnews::TABNEWS_2,
			sections::HORSDC => tabnews::TABNEWS_3,
			sections::INDE   => tabnews::TABNEWS_4,
			sections::MARVEL => tabnews::TABNEWS_5,
		];

		foreach ($covers as $section => $cover)
		{
			$tab_to_update = $tabnews_order[$section];
			$sql = "SELECT config_value FROM " . $this->table_config_text . " WHERE " . $this->db->sql_build_array('SELECT', ['config_name' => $tab_to_update]);
			$result = $this->db->sql_query($sql);
			if (!$result)
			{
				continue;
			}
			$row = $this->db->sql_fetchrow($result);
			$this->db->sql_freeresult($result);
			$tabnews = html_entity_decode($row['config_value']);

			$first_pattern = '/(?s)<\!-- BEGIN COVER -->\s(.*)\s<\!-- END COVER -->/';
			preg_match($first_pattern, $tabnews, $first_search);

			$second_pattern = "/(?:(<a (?:(?!<\/a>).)*<\/a>)+)/";
			preg_match_all($second_pattern, $first_search[1], $tabnews_covers);

			if (isset($tabnews_covers[0]) && !empty($tabnews_covers[0]))
			{
				$tabnews_covers = $tabnews_covers[0];
				$count_actual_covers = count($tabnews_covers);
				if (9 < $count_actual_covers)
				{
					continue;
				}

				if (9 === $count_actual_covers)
				{
					$covers_toremove = $tabnews_covers;
					$count_toremove = count($cover);
					array_splice($covers_toremove, 0, 9 - $count_toremove);
					array_splice($tabnews_covers, -$count_toremove, $count_toremove);
				}

				$tabnews_covers = array_merge($cover, $tabnews_covers);
				$tabnew_replace = self::BEGIN_TAG . implode("\n", $tabnews_covers) . self::END_TAG;
				$replace = preg_replace($first_pattern, $tabnew_replace, $tabnews);
				$new_tabnews = [
					"config_value" => htmlentities($replace),
				];

				$sql = "UPDATE " . $this->table_config_text . " SET " . $this->db->sql_build_array('UPDATE', $new_tabnews) . " WHERE config_name = '$tab_to_update'";

				$result = $this->db->sql_query($sql);
				if (!$result)
				{
					return false;
				}
			}
		}

		return true;
	}

	public function remove_files(array $files): void
	{
		foreach ($files as $file)
		{
			$this->ady_core->remove_file(paths::COVERS_PATH . '/' . $file);
		}
	}

	public function delete_request(array $ids): bool
	{
		$sql = "DELETE FROM " . $this->table_changecover_toapprove . " WHERE " . $this->db->sql_in_set('id', $ids);
		$this->db->sql_query($sql);

		return !!$this->db->sql_affectedrows();
	}

	/**
	 * @throws uploadException
	 */
	public function upload_cover(array $file): string
	{
		$this->ady_core->check_image($file);

		$image = $this->ady_core->resize_image($file['tmp_name'], $this->ady_core::WIDTH, self::WIDTH);

		return $this->ady_core->save_image($file, $image);
	}
}
