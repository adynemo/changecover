<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2023 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */


namespace ady\changecover\core;

use ady\changecover\constant\comicsFormats;
use ady\changecover\constant\paths;
use ady\changecover\constant\sections;
use ady\changecover\dto\publication\request as pub_request;
use phpbb\db\driver\driver_interface;
use phpbb\user;

class publish
{
	private core $ady_core;
	private user $user;
	private users $users;
	private driver_interface $db;
	private string $root_path;
	private string $php_ext;

	public function __construct(
		core $ady_core,
		user $user,
		users $users,
		driver_interface $db,
		string $root_path,
		string $php_ext
	)
	{
		$this->ady_core = $ady_core;
		$this->user = $user;
		$this->users = $users;
		$this->db = $db;
		$this->root_path = $root_path;
		$this->php_ext = $php_ext;
	}

	public function post_comic(array $comic, bool $new): string
	{
		if (!class_exists('parse_message'))
		{
			include($this->root_path . 'includes/message_parser.' . $this->php_ext);
		}
		if (!function_exists('submit_post'))
		{
			include($this->root_path . 'includes/functions_posting.' . $this->php_ext);
		}

		if (!$new)
		{
			$entity = $this->find_post((int) $comic['topic_id']);
		}
		else
		{
			$entity = $this->find_forum((int) $comic['forum_id']);
		}
		if (null === $entity)
		{
			throw new \Exception('DCTNEWS_REQUEST_ERROR_PUBLISH_POST_ENTITY_NOT_FOUND');
		}
		$poll = [];
		$message_parser = new \parse_message($comic['message']);
		$message_parser->parse(true, true, true, true, false);
		$topic_title = $entity['topic_title'] ?? $comic['topic_title'];
		$data = [
			'forum_id'       => (int) $entity['forum_id'],
			'icon_id'        => false,
			'enable_sig'     => true,
			'enable_bbcode'  => true,
			'enable_smilies' => true,
			'enable_urls'    => true,

			'message_md5' => md5($message_parser->message),

			'bbcode_bitfield' => $message_parser->bbcode_bitfield,
			'bbcode_uid'      => $message_parser->bbcode_uid,
			'message'         => $message_parser->message,
			'attachment_data' => $message_parser->attachment_data,
			'filename_data'   => $message_parser->filename_data,

			'post_edit_locked' => 0,
			'topic_title'      => $topic_title,
			'topic_desc'       => $entity['forum_desc'] ?? '',
			'poster_id'        => $this->user->data['user_id'],
			'notify_set'       => false,
			'notify'           => true,
			'post_time'        => time(),
			'forum_name'       => $entity['forum_name'],
			'enable_indexing'  => true,

			'force_approved_state' => ITEM_APPROVED,
			'force_visibility'     => ITEM_APPROVED,
		];

		$mode = 'post';
		if (!$new)
		{
			$mode = 'reply';
			$data['topic_id'] = $entity['topic_id'];
		}

		$url = submit_post(
			$mode,
			$topic_title,
			$this->user->data['username'],
			$entity['topic_type'] ?? POST_NORMAL,
			$poll,
			$data
		);

		if (false === $url) {
			throw new \Exception('DCTNEWS_REQUEST_ERROR_PUBLISH_POST');
		}

		parse_str(parse_url($url)['query'], $url);

		return $new ? 
			sprintf('%1$s/viewtopic.php?t=%2$d', generate_board_url(), $url['t']) : 
			sprintf('%1$s/viewtopic.php?p=%2$d#p%2$d', generate_board_url(), $url['p']);
	}

	public function create_reply_post(pub_request\request $request): string
	{
		$message = <<<EOT
[center]
[size=200]%s[/size]

[img]%s[/img]
%s
%s
[url=%s]⬆️ Lien disponible au premier post ⬆️[/url]
[/center]
EOT;
		$users_id = [
			...$request->team->trad,
			...$request->team->c1,
			...$request->team->edit,
			...$request->team->c2,
		];
		$users = $this->users->find_users_by_ids($users_id);
		$teamSection = '';
		if ([] !== $users)
		{
			$team = $this->parse_team($request->team, $users);
			$teamSection = <<<EOT

[b]
Trad : %s
C1 : %s 
Edit : %s
C2 : %s
[/b]
EOT;
			$teamSection = sprintf(
				$teamSection,
				$team['trad'],
				$team['c1'],
				$team['edit'],
				$team['c2']
			);
		}

		return sprintf(
			$message,
			$this->build_title(
				$request->serie->title,
				$request->issue->title,
				$request->issue->number,
				$request->issue->format
			),
			$this->ady_core->get_board_url() . paths::COVERS_PATH . '/' . $request->issue->cover,
			$teamSection,
			$request->description,
			$request->serie->url
		);
	}

	public function create_new_post(pub_request\request $request): string
	{
		$message = <<<EOT
[center]
[size=200]%s[/size]

[img]%s[/img]
%s
%s
[url=%s][img]%s[/img][/url]
[/center]
EOT;
		$users_id = [
			...$request->team->trad,
			...$request->team->c1,
			...$request->team->edit,
			...$request->team->c2,
		];
		$users = $this->users->find_users_by_ids($users_id);
		$teamSection = '';
		if ([] !== $users)
		{
			$team = $this->parse_team($request->team, $users);
			$teamSection = <<<EOT

[b]
Trad : %s
C1 : %s 
Edit : %s
C2 : %s
[/b]
EOT;
			$teamSection = sprintf(
				$teamSection,
				$team['trad'],
				$team['c1'],
				$team['edit'],
				$team['c2']
			);
		}

		$share_image = $this->ady_core->get_board_url() . (
			comicsFormats::TPB !== $request->issue->format ?
				'images/ady/dctbbcode/oc_issues.png' :
				'images/ady/dctbbcode/oc_tpb.png'
			);

		return sprintf(
			$message,
			$request->serie->title,
			$this->ady_core->get_board_url() . paths::COVERS_PATH . '/' . $request->issue->cover,
			$teamSection,
			$request->description,
			$request->serie->share_link,
			$share_image
		);
	}

	public function build_title(
		string $serie_title,
		?string $issue_title,
		string $issue_number,
		?int $comics_format
	): string
	{
		if (null === $comics_format)
		{
			return $serie_title . ' ' . $issue_number;
		}

		$comicsFormatPrefix = [
			comicsFormats::ISSUE  => '#',
			comicsFormats::ANNUAL => 'Annual #',
			comicsFormats::TPB    => 'Vol. ',
		];
		$title = $serie_title;
		if ('' !== $issue_number && comicsFormats::ONE_SHOT !== $comics_format)
		{
			$title .= ' ' . $comicsFormatPrefix[$comics_format] . $issue_number;
		}
		if (!empty($issue_title))
		{
			$title .= ' : ' . $issue_title;
		}
		if (comicsFormats::ONE_SHOT === $comics_format)
		{
			$title .= ' (One Shot)';
		}

		return $title;
	}

	public function build_forum_select(): string
	{
		if (!function_exists('make_forum_select'))
		{
			include($this->root_path . 'includes/functions_admin.' . $this->php_ext);
		}

		$forums = make_forum_select(false, false, false, true, true, true, true);

		while ($forum = array_shift($forums)) {
			// id of the "Scantrad DCT-Team" to remove forums before scantrads
			if ('11' === $forum['forum_id']) break;
		}
		while ($forum = array_pop($forums)) {
			// id of the "DaCreaTeam" to remove forums after scantrads
			if ('205' === $forum['forum_id']) break;
		}

		$html = '';
		$parent = '';
		$map = [
			'26' => sections::HORSDC,
			'188' => sections::DC,
			'130' => sections::MARVEL,
			'40' => sections::INDE,
		];
		foreach ($forums as $forum) {
			$forum['disabled'] = '11' === $forum['parent_id'];
			$parent = $map[$forum['forum_id']] ?? $map[$forum['parent_id']] ?? $parent;

			$disabled_html = $forum['disabled'] ? 'disabled="disabled" class="disabled-option"' : $forum['selected'];
			$html .= <<<EOT
<option value="{$forum['forum_id']}"
		$disabled_html
		data-ady-main-forum="$parent"
>
{$forum['padding']}{$forum['forum_name']}
</option>
EOT;
		}

		return $html;
	}

	private function parse_team(pub_request\team $team, array $users): array
	{
		$parsed = [
			'trad' => '',
			'c1'   => '',
			'edit' => '',
			'c2'   => '',
		];
		$div = ' / ';
		$serialized = $team->toArray();
		foreach ($serialized as $post => $ids)
		{
			foreach ($ids as $id)
			{
				$user = $users[$id];
				$parsed[$post] .= $user['username'] . $div;
			}
			$parsed[$post] = rtrim($parsed[$post], $div);
		}

		return $parsed;
	}

	private function find_post(int $id): ?array
	{
		$topicsTable = TOPICS_TABLE;
		$forumsTable = FORUMS_TABLE;
		$sql = <<<EOT
SELECT t.*, f.forum_name
FROM $topicsTable t 
INNER JOIN $forumsTable f ON f.forum_id = t.forum_id
WHERE t.topic_id = $id
EOT;
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return null;
		}
		$rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		return $rows[0] ?? null;
	}

	private function find_forum(int $id): ?array
	{
		$table = FORUMS_TABLE;
		$sql = <<<EOT
SELECT f.*
FROM $table f
WHERE f.forum_id = $id
EOT;
		$result = $this->db->sql_query($sql);
		if (!$result)
		{
			return null;
		}
		$rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		return $rows[0] ?? null;
	}
}