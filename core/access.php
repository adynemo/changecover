<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\core;

use ady\changecover\constant\permissions;
use phpbb\auth\auth;
use phpbb\config\config;
use phpbb\controller\helper;
use phpbb\template\template;
use phpbb\user;

class access
{
	private covers $ady_covers;
	private template $template;
	private auth $auth;
	private helper $helper;
	private user $user;
	private config $config;

	public function __construct(
		covers $ady_covers,
		template $template,
		auth $auth,
		helper $helper,
		user $user,
		config $config
	)
	{
		$this->ady_covers = $ady_covers;
		$this->template = $template;
		$this->auth = $auth;
		$this->helper = $helper;
		$this->user = $user;
		$this->config = $config;
	}

	public function assign_access()
	{
		$cover_admin = $this->auth->acl_get(permissions::U_COVER_APPROVER);
		$cover_team = $this->auth->acl_get(permissions::U_COVER_REQUESTER);
		$tabnews_team = $this->auth->acl_get(permissions::U_NEWS_REQUESTER);
		$tabnews_admin = $this->auth->acl_get(permissions::A_NEWS_MANAGER);

		$pepper = false;
		if (class_exists(\ady\pepper\constant\permissions::class) && $this->is_route_exist('ady_pepper_projects_organizer'))
		{
			$pepper = $this->auth->acl_get(\ady\pepper\constant\permissions::U_ORGANIZER);
		}

		$count_approvals = 0;
		if ($cover_admin)
		{
			$count_approvals = $this->ady_covers->count_cover_to_approve();
		}

		$has_access = $this->user->data['is_registered'];

		$template_data = [
			'U_CHANGECOVER_APPROVER'          => $cover_admin,
			'U_CHANGECOVER_REQUESTER'         => $cover_team,
			'U_CHANGECOVER_TABNEWS_REQUESTER' => $tabnews_team,
			'A_CHANGECOVER_TABNEWS_APPROVER'  => $tabnews_admin,
			'U_ADY_PEPPER_ORGANIZER'          => $pepper,
			'HAS_ACCESS'                      => $has_access,
			'NOTIFICATIONS'                   => $count_approvals,
		];

		$this->template->assign_vars($template_data);
	}

	public function set_csrf_token(string $token_name): void
	{
		$now = (new \DateTimeImmutable())->getTimestamp();
		$token_sid = ($this->user->data['user_id'] == ANONYMOUS && !empty($this->config['form_token_sid_guests'])) ? $this->user->session_id : '';
		$token = sha1($now . $this->user->data['user_form_salt'] . $token_name . $token_sid);

		$this->template->assign_vars([
			"TOKEN" => $token,
			"NOW"   => $now,
		]);
	}

	public function get_config(string $key): ?string
	{
		return $this->config[$key] ?? null;
	}

	private function is_route_exist(string $route): bool
	{
		$exists = true;
		try
		{
			$this->helper->route($route);
		}
		catch (\Throwable $exception)
		{
			$exists = false;
		}

		return $exists;
	}
}
