<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2021 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\core;

class locg
{
	public const PREVIEWS_TYPE = "previews";
	public const WEEK_TYPE = "week";

	public const DOMAIN_NAME = "https://leagueofcomicgeeks.com";
	private const URL_API = self::DOMAIN_NAME . "/comic/get_comics?addons=1&list=releases&list_option=&list_refinement=firsts&date_type=%s&date=%s&date_end=&series_id=&user_id=0&title=&view=list&format[]=1&format[]=6%s&character=&order=pulls";

	public function fetch(\DateTime $date, array $filters, string $type): array
	{
		if (self::PREVIEWS_TYPE === $type)
		{
			/**
			 * Monthly release for #1's on LOCG
			 * M-0 = LOCG M-3
			 */
			$date = $date->modify('3 month ago');
		}

		$filter = '';
		if ([] !== $filters)
		{
			$filter = '&' . implode('&', $filters);
		}

		$url = sprintf(self::URL_API, $type, $date->format('m/d/Y'), $filter);

		$news = file_get_contents($url);
		$news = json_decode($news);

		$list_news = $news->list;
		$pattern = '/<img class="lazy"\s.+data\-src="([^"]+)"\salt="([^"]+[^c]+[^>]+[^c]+){3}class="title.+data\-sorting="([^"]+)[^"]+"[^c]+[^>]+[^c]+href="([^"]+)/';
		preg_match_all($pattern, $list_news, $all_news);

		$results = [];
		if ([] !== $all_news[0])
		{
			$results = [
				'cover' => $all_news[1],
				'title' => $all_news[3],
				'url'   => $all_news[4],
			];
		}
		unset($all_news);

		return $results;
	}
}
