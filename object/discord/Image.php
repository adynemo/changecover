<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\object\discord;

class Image implements \JsonSerializable
{
	public string $url;

	public function jsonSerialize(): array
	{
		return [
			'url' => $this->url,
		];
	}
}
