<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\object\discord;

class Release implements \JsonSerializable
{
	public string $title;
	public string $url;
	public int $color;
	public Image $image;

	public function jsonSerialize(): array
	{
		return [
			'title' => $this->title,
			'url'   => $this->url,
			'color' => $this->color,
			'image' => $this->image,
		];
	}
}
