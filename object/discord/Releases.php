<?php

/**
 * @package   phpBB Extension - ChangeCover
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\changecover\object\discord;

class Releases implements \JsonSerializable
{
	private array $embeds;

	public function getEmbeds(): array
	{
		return $this->embeds;
	}

	public function addEmbed(Release $release): self
	{
		$this->embeds[] = $release;

		return $this;
	}

	public function jsonSerialize(): array
	{
		return [
			'embeds' => $this->embeds,
		];
	}
}
