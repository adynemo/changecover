(function() {
	const elements = document.querySelectorAll('.adychangecover.comics-index .indices .element');
	const searchInput = document.getElementById('search');

	if (searchInput) {
		searchInput.addEventListener('input', function(e) {
			const text = e.target.value;
			const regexp = new RegExp(text, 'i');
			Array.prototype.forEach.call(elements, function(element) {
				if (!element.querySelector('.title').innerText.match(regexp)) {
					element.style.display = 'none';
				} else {
					element.style.display = 'flex';
				}
			});
		});
	}

	const legends = document.querySelectorAll('.adychangecover.comics-index .index-legend:not(.no-js) .element');
	let currentFilter = '';
	for (const legend of legends) {
		legend.addEventListener('click', function(e) {
			const button = e.target;
			const filter = button.dataset.filter;
			if (currentFilter === filter) {
				currentFilter = '';
				button.classList.remove('active');
				Array.prototype.forEach.call(elements, function(element) {
					element.style.display = 'flex';
				});
			} else {
				currentFilter = filter;
				Array.prototype.forEach.call(legends, function(legend) {
					legend.classList.remove('active');
				});
				button.classList.add('active');
				Array.prototype.forEach.call(elements, function(element) {
					if (('hidden' !== filter && filter !== element.dataset.status) ||
						('hidden' === filter && !element.dataset.hidden)
					) {
						element.style.display = 'none';
					} else {
						element.style.display = 'flex';
					}
				});
			}
		})
	}

	function outerWidth(el) {
		const style = getComputedStyle(el);

		return (
			el.getBoundingClientRect().width +
			parseFloat(style.marginLeft) +
			parseFloat(style.marginRight)
		);
	}

	function isScrollable(element) {
		return element.scrollWidth > element.clientWidth || element.scrollHeight > element.clientHeight;
	}

	(function centerActiveCharacter() {
		const out = document.querySelector('.adychangecover.comics-index .link-pagination.characters');
		if (!isScrollable(out)) {
			return;
		}
		const all = out.querySelectorAll('.button');
		const tar = out.querySelector('.active');
		const x = out.offsetWidth;
		const y = outerWidth(tar);
		const z = [...all].indexOf(tar);
		let q = 0;
		//Just need to add up the width of all the elements before our target.
		for (let i = 0; i < z; i++) {
			q += outerWidth(all[i]);
		}
		out.scrollLeft = Math.max(0, q - (x - y) / 2);
	})()
})()
