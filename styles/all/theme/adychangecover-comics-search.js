// todo: rename file
// todo: rename serie to series
(function () {
	/**
	 * <<<INIT
	 */
	const vars = document.getElementById('ady-js-vars');
	const token = vars.dataset.adyToken;
	const date = vars.dataset.adyDate;
	const apiSearchUsers = vars.dataset.adySearchUsers;
	const apiSearchSeries = vars.dataset.adySearchSeries;
	const form = document.getElementsByTagName('form')[0];
	const urlCloudField = document.getElementById('url-cloud');
	const serieChangeEvent = new CustomEvent('serie-change');
	const typeInput = document.getElementById('publication-type');
	const publishPostInput = document.getElementById('publication-action');
	const serieUrlInput = document.getElementById('serie-url');
	const fields = form.querySelectorAll('.row.fields .field');
	const resetButton = document.getElementById('reset');
	const oldSerieTitleContainer = document
		.getElementById('serie-title-input-container')
		.querySelector('.show-results');
	const previewContainer = document.getElementById('preview-title');
	const serieIdInput = document.getElementById('serie-id');
	const newSerieTitleInput = document.getElementById('serie-title');
	const issueNumberInput = document.getElementById('issue-number');
	const issueTitleInput = document.getElementById('issue-title');
	const comicsFormatSelect = document.getElementById('comics-format');
	const editorSelect = document.getElementById('editor');
	const forumSelect = document.getElementById('id-forum');
	const comicsFormatPrefix = {
		1: '#',
		3: 'Annual #',
		4: 'Vol. ',
	};
	/**
	 * INIT>>>
	 */

	/**
	 * <<<AUTOCOMPLETE
	 */
	const selectEditor = id => editorSelect.querySelector(`option[value="${id}"]`).selected = true;

	function sendForm(formData, endpoint, statusCode, callback) {
		const request = new XMLHttpRequest();
		formData.append('creation_time', date);
		formData.append('form_token', token);
		request.open('POST', endpoint);
		request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

		request.onreadystatechange = function () {
			if (request.readyState === XMLHttpRequest.DONE) {
				if (statusCode === request.status) {
					callback(request)
				} else {
					// todo: handle error 
					// errorMessage.textContent = i18n.generalError;
				}
			}
		}

		request.send(formData);
	}

	function removeChip(e, field = undefined) {
		e.currentTarget.remove();
		if (!field) {
			return;
		}
		const id = e.currentTarget.dataset.id;
		const ids = JSON.parse(field.value);
		const i = ids.indexOf(id);
		if (i === -1) {
			return;
		}
		ids.splice(i, 1);
		field.value = JSON.stringify(ids);
	}

	function chooseSerie(e, fieldId) {
		const item = JSON.parse(e.currentTarget.dataset.item);
		const container = e.currentTarget.parentElement.parentElement.parentElement;
		const showContainer = container.querySelector('div.show-results');
		const autocomplete = container.querySelector('.autocomplete');
		const field = document.getElementById(fieldId);
		field.value = item.id;
		autocomplete.value = '';
		showContainer.type = 'text';
		const chip = document.createElement('div');
		chip.classList.add('item', 'chip', 'serie');
		chip.innerHTML = item.title;
		chip.dataset.value = item.id;
		showContainer.innerHTML = chip.outerHTML;
		const idField = document.getElementById('serie-id');
		idField.value = item.id;
		const statusField = document.getElementById('trad-status');
		let wipOption;
		for (const node of statusField.children) {
			node.selected = false;
			if ('1' === node.value) {
				wipOption = node;
			}
			if (item.status === node.value) {
				node.selected = true;
				wipOption = undefined;
				break;
			}
		}
		if (wipOption) {
			wipOption.selected = true;
		}
		field.dispatchEvent(serieChangeEvent);
	}

	function chooseUser(e, fieldId) {
		const user = JSON.parse(e.currentTarget.dataset.item);
		const container = e.currentTarget.parentElement.parentElement.parentElement;
		const showContainer = container.querySelector('div.show-results');
		const autocomplete = container.querySelector('.autocomplete');
		const field = document.getElementById(fieldId);
		if (field.value.length === 0) {
			field.value = '[]';
		}
		const ids = JSON.parse(field.value);
		ids.push(user.id);
		field.value = JSON.stringify(ids);
		autocomplete.value = '';
		showContainer.type = 'text';
		const chip = document.createElement('div');
		chip.classList.add('item', 'chip');
		chip.dataset.id = user.id;
		chip.innerHTML = user.avatar;
		chip.title = 'Cliquez pour supprimer';
		chip.addEventListener('click', (e) => {
			removeChip(e, field)
		});
		const username = document.createElement('span');
		username.textContent = user.username;
		chip.append(username);
		chip.dataset.value = user.id;
		showContainer.append(chip);
	}

	function search(containerId, fieldId, apiUser) {
		const container = document.querySelector('#' + containerId + ' .autocomplete-field');
		const searchInput = container.querySelector('input.autocomplete');
		const searchEndpoint = apiUser ? apiSearchUsers : apiSearchSeries;

		searchInput.addEventListener('input', function (e) {
			const searchData = searchInput.value;

			let choice;
			while (choice = container.getElementsByClassName('autocomplete-result')[0]) {
				choice.remove();
			}
			if (searchData.length >= 3) {
				const formData = new FormData();
				formData.append('q', searchData);
				sendForm(formData, searchEndpoint, 200, function (request) {
					const data = JSON.parse(request.responseText);
					const wrapper = document.createElement('div');
					wrapper.className = "autocomplete-result";
					container.appendChild(wrapper);
					Object.keys(data).map(function (key, i) {
						const item = data[i];

						const searchResultsContainer = document.createElement('div');
						searchResultsContainer.setAttribute('class', 'row');
						searchResultsContainer.dataset.item = JSON.stringify(item);
						searchResultsContainer.addEventListener('click', (e) => {
							if (apiUser) {
								chooseUser(e, fieldId);
								return;
							}
							chooseSerie(e, fieldId);
							if (item.editor) selectEditor(item.editor);
						});

						if (apiUser) {
							const username = document.createElement('span');
							username.textContent = item.username;
							const avatar = document.createElement('span');
							avatar.innerHTML = item.avatar;

							wrapper.appendChild(searchResultsContainer);
							searchResultsContainer.appendChild(avatar);
							searchResultsContainer.appendChild(username);
							return;
						}

						const serie = document.createElement('span');
						serie.textContent = item.title;
						wrapper.appendChild(searchResultsContainer);
						searchResultsContainer.appendChild(serie);
						if (item.status === "3") {
							// todo: warn user that the chosen serie is ended
						}
					});

					function closeAutocomplete(e) {
						if (!wrapper.contains(e.currentTarget)) {
							wrapper.remove();
						}
					}

					document.addEventListener('click', closeAutocomplete)
				});
			}
		})
	}

	search('serie-title-input-container', 'serie-id', false);
	search('trad-input-container', 'team-trad', true);
	search('c1-input-container', 'team-c1', true);
	search('edit-input-container', 'team-edit', true);
	search('c2-input-container', 'team-c2', true);
	
	forumSelect.addEventListener('change', function (e) {
		const editorId = this.options[this.selectedIndex].dataset.adyMainForum;
		selectEditor(editorId);
	});
	/**
	 * AUTOCOMPLETE>>>
	 */

	/**
	 * <<<RESET
	 */
	function resetForm() {
		const inputs = form.getElementsByTagName('input');
		for (const input of inputs) {
			if (input.type === 'submit') continue;
			if (input.type === 'checkbox') {
				input.checked = false;
				continue;
			}
			input.value = '';
		}
		const textareas = form.getElementsByTagName('textarea');
		for (const textarea of textareas) {
			textarea.innerText = '';
		}
		const options = form.getElementsByTagName('option');
		for (const option of options) {
			option.selected = false;
			if (undefined !== option.dataset.adyOptionDefault) {
				option.selected = true;
			}
		}
		const jobs = form.querySelectorAll('.show-results');
		for (const job of jobs) {
			job.textContent = '';
		}
	}

	resetForm();
	resetButton.addEventListener('click', resetForm);
	/**
	 * RESET>>>
	 */

	/**
	 * <<<TOGGLE FIELDS
	 * publication types:
	 * 1. new issue in an existing series
	 * 2. new series
	 * 
	 * 3. publish post
	 * 4. don't publish post
	 */
	let firstIssue = typeInput.checked;
	let publishPost = publishPostInput.checked;

	function toggleFirstIssue() {
		firstIssue = typeInput.checked;
		for (const field of fields) {
			const pubType = field.dataset.adyPubType;
			if ((firstIssue && pubType === '1') || (!firstIssue && pubType === '2')) {
				field.style.display = 'block';
			} else if ((firstIssue && pubType === '2') || (!firstIssue && pubType === '1')) {
				field.style.display = 'none';
				field.querySelectorAll('input').forEach(input => input.value = '');
			}
		}
		const serieField = document.getElementById('serie-title-input-container');
		serieField.querySelector('.show-results').innerText = '';
		resetForumValidation(urlCloudField);
		newSerieTitleInput.required = firstIssue;
		urlCloudField.required = firstIssue;
	}

	function togglePublishPost() {
		publishPost = publishPostInput.checked;
		for (const field of fields) {
			const pubType = field.dataset.adyPubType;
			if ((publishPost && pubType === '3') || (!publishPost && pubType === '4')) {
				field.style.display = 'block';
			} else if ((publishPost && pubType === '4') || (!publishPost && pubType === '3')) {
				field.style.display = 'none';
				field.querySelectorAll('input').forEach(input => input.value = '');
			}
		}
		resetForumValidation(serieUrlInput);
		serieUrlInput.required = !publishPost;
		
		if (firstIssue) {
			resetForumValidation(urlCloudField);
			urlCloudField.required = publishPost;
			urlCloudField.parentElement.style.display = publishPost ? 'block' : 'none';
		}
	}
	
	function toggleOptions() {
		toggleFirstIssue();
		togglePublishPost();
	}

	typeInput.addEventListener('change', function () {
		toggleOptions();
		const previewSpan = previewContainer.querySelector('span.text');
		previewSpan.innerText = '';
	});
	publishPostInput.addEventListener('change', toggleOptions);
	publishPostInput.checked = 'checked';
	toggleOptions();
	/**
	 * TOGGLE FIELDS>>>
	 */

	/**
	 * <<<VALIDATION
	 */
	function resetForumValidation(field) {
		const icon = field.nextElementSibling;
		const span = icon.nextElementSibling;
		icon.style.display = 'none';
		icon.classList.remove('fa-check-circle', 'fa-times-circle', 'fa-warning');
		span.classList.remove('warning');
		span.style.display = 'none';
		span.innerText = '';
	}

	function displayFieldError(message, span, icon) {
		if (icon) {
			icon.style.display = 'block';
			icon.classList.add('fa-times-circle');
		}
		span.style.display = 'block';
		span.innerText = message;

	}

	function displayUrlCloudWarning(_, span, icon) {
		if (icon) {
			icon.style.display = 'block';
			icon.classList.add('fa-warning');
		}
		span.style.display = 'block';
		span.classList.add('warning');
		span.innerText = "Attention, ce n'est pas une URL du OwnCloud DC-Trad.";

	}

	function validateUrl(target, host, callbackHostError) {
		resetForumValidation(target);
		let callback = displayFieldError;
		const icon = target.nextElementSibling;
		const span = icon.nextElementSibling;
		try {
			const url = new URL(target.value);
			if (host !== url.host) {
				callback = typeof callbackHostError === 'function' ? callbackHostError : displayFieldError;
				throw new Error();
			}
			icon.style.display = 'block';
			icon.classList.add('fa-check-circle');
			icon.title = "URL valide";
		} catch {
			callback("L'URL est invalide", span, icon);
		}
	}

	urlCloudField.addEventListener('input', e => validateUrl(e.currentTarget, 'dl.dctrad.fr', displayUrlCloudWarning));
	serieUrlInput.addEventListener('input', e => validateUrl(e.currentTarget, 'dctrad.fr'));

	function beforeSubmit(e) {
		comicsFormatSelect.childNodes.forEach(option => {
			if (
					(true === option.selected) && 
					(["1", "3"].includes(option.value)) && 
					(issueNumberInput.value.length === 0))
				{
				e.preventDefault();
				const span = issueNumberInput.nextElementSibling;
				displayFieldError("Un numéro est obligatoire avec les formats 'issue' et 'annual'", span);
			}
		});
	}
	form.addEventListener('submit', beforeSubmit);
	/**
	 * VALIDATION>>>
	 */

	/**
	 * <<<PREVIEW
	 */
	function previewTitle() {
		let oldSerieTitle = oldSerieTitleContainer.innerText;
		const newSerieTitle = newSerieTitleInput.value;
		const serieTitle = oldSerieTitle || newSerieTitle || '';
		const issueNumber = issueNumberInput.value;
		const issueTitle = issueTitleInput.value;
		let comicsFormat;
		comicsFormatSelect.childNodes.forEach(option => {
			if (true === option.selected) {
				comicsFormat = option.value;
			}
		});

		let title = serieTitle;
		if (issueNumber && '2' !== comicsFormat) {
			title += ` ${comicsFormatPrefix[comicsFormat]}${issueNumber}`;
		}
		if (issueTitle) {
			title += ` : ${issueTitle}`;
		}
		if ('2' === comicsFormat) {
			title += ' (One Shot)';
		}

		const span = previewContainer.querySelector('span.text');
		span.innerText = title;
	}

	serieIdInput.addEventListener('serie-change', previewTitle);
	newSerieTitleInput.addEventListener('input', previewTitle);
	issueNumberInput.addEventListener('input', () => {
		previewTitle();
		const span = issueNumberInput.nextElementSibling;
		span.style.display = 'none';
		span.innerText = '';
	});
	issueTitleInput.addEventListener('input', previewTitle);
	comicsFormatSelect.addEventListener('input', previewTitle);
	/**
	 * PREVIEW>>>
	 */

	/**
	 * <<<FILE UPLOAD
	 */
	function upload() {
		const fileSelect = document.getElementById('file-upload');
		const fileDrag = document.getElementById('file-drag');
		fileSelect.addEventListener('change', fileSelectHandler, false);
		fileDrag.addEventListener('dragover', fileDragHover, false);
		fileDrag.addEventListener('dragleave', fileDragHover, false);
		fileDrag.addEventListener('drop', fileSelectHandler, false);

		function fileDragHover(e) {
			e.stopPropagation();
			e.preventDefault();
			fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
		}

		function fileSelectHandler(e) {
			const files = e.target.files || e.dataTransfer.files;
			fileDragHover(e);
			const dT = new DataTransfer();
			for (let i = 0, f; f = files[i]; i++) {
				dT.items.add(f);
				parseFile(f);
			}
			fileSelect.files = dT.files;
		}

		function output(msg) {
			const m = document.getElementById('messages');
			m.innerHTML = msg;
		}

		function parseFile(file) {
			output('<strong>' + encodeURI(file.name) + '</strong>');
			const imageName = file.name;

			const isGood = (/\.(?=jpg|png|jpeg)/gi).test(imageName);
			if (isGood) {
				document.getElementById('start').classList.add("hidden");
				document.getElementById('response').classList.remove("hidden");
				document.getElementById('notimage').classList.add("hidden");
				document.getElementById('file-image').classList.remove("hidden");
				document.getElementById('file-image').src = URL.createObjectURL(file);
				return;
			}
			document.getElementById('file-image').classList.add("hidden");
			document.getElementById('notimage').classList.remove("hidden");
			document.getElementById('start').classList.remove("hidden");
			document.getElementById('response').classList.add("hidden");
			document.getElementById("file-upload-form").reset();
		}
	}
	upload();
	/**
	 * FILE UPLOAD>>>
	 */
})()
