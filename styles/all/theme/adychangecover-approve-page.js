(function() {
	const elements = document.querySelectorAll('.adychangecover.approve .element');

	for (const element of elements) {
		const approveInput = element.querySelector('.checkbox-input.approve');
		approveInput.addEventListener('click', function(e) {
			if (e.target.checked) {
				element.classList.remove('refused');
			} else {
				element.classList.add('refused');
			}
		});
	}
})();
